$(document).ready(function(){
$.validator.setDefaults({ ignore: ":hidden:not(select)" })						   
$("#add_service_enquiry").validate({
						 errorElement: "label",
								  rules:{
									 /* enquiry_type_id:{required:true
										 				},	*/
									 /* 	*/
									 /* product_drop:{required:true
										 				},*/
														service_drop:{required:true
										 				},
									  	 customer_name:{required:true
										 				},	
									 	 customer_email:{required:true,email:true},
										 customer_mobile:{required:true,minlength:10,maxlength:10},
										 enquiry_subject:{required:true},
										 enquiry_message:{required:true}
	   									},
			messages: {
				/*enquiry_type_id:{
				required:"Please select enquiry type."
			},	*/
			/*
			product_drop:{
				required:"Please select product."
			},	*/
			service_drop:{
				required:"Please select service."
			},	
			customer_name:{
				required:"Please enter name."
			},	
			customer_email:{
				required:"Please enter email address.",
				email:"please enter valid email address."
				     },
			customer_mobile:{
				required:"Please enter valid mobile number."
				     },
			enquiry_subject:{
				required:"Please enter area."
				     },
		 	enquiry_message:{
				required:"Please enter enquiry message."
				     }
		},
		errorPlacement: function(error, element) 
					{
							error.appendTo(element.parent());
					},
								  submitHandler:function(form){
							
            $.ajax({
				   
                    url:"Home/enquiry_submission",
                    type:'POST',
                    data:$(form).serialize(),
                    success:function(result){
					//	$("#products").hide();
 					//	$("#services").hide();
						$("#add_service_enquiry")[0].reset();
                        $("#service_response").html(result);
					}

            });
      
								  }
								  });
$("#add_product_enquiry").validate({
						 errorElement: "label",
								  rules:{
									 /* enquiry_type_id:{required:true
										 				},	*/
									 /* service_drop:{required:true
										 				},	*/
									 /* */
									 product_drop:{required:true
										 				},
									  	 customer_name:{required:true
										 				},	
									 	 customer_email:{required:true,email:true},
										 customer_mobile:{required:true,minlength:10,maxlength:10},
										 enquiry_subject:{required:true},
										 enquiry_message:{required:true}
	   									},
			messages: {
				/*enquiry_type_id:{
				required:"Please select enquiry type."
			},	*/
			/*service_drop:{
				required:"Please select service."
			},	
				*/
				product_drop:{
				required:"Please select product."
			},
			customer_name:{
				required:"Please enter name."
			},	
			customer_email:{
				required:"Please enter email address.",
				email:"please enter valid email address."
				     },
			customer_mobile:{
				required:"Please enter valid mobile number."
				     },
			enquiry_subject:{
				required:"Please enter area."
				     },
		 	enquiry_message:{
				required:"Please enter enquiry message."
				     }
		},
		errorPlacement: function(error, element) 
					{
							error.appendTo(element.parent());
					},
								  submitHandler:function(form){
							
            $.ajax({
				   
                    url:"Home/enquiry_submission",
                    type:'POST',
                    data:$(form).serialize(),
                    success:function(result){
					//	$("#products").hide();
 					//	$("#services").hide();
						$("#add_product_enquiry")[0].reset();
                        $("#product_response").html(result);
					}

            });
      
								  }
								  });
});