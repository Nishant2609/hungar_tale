$(document).ready(function(){
$("#contact_form").validate({
	errorElement: "div",
						 rules:{
									 tg_txt_name:{required:true},	
									 tg_txt_email:{required:true,email:true},
									 tg_txt_message:{required:true}
								},
			messages: {
			tg_txt_name:{
				required:"Please enter name."
				},	
			tg_txt_email:{
				required:"Please enter email address.",
				email:"please enter valid email address."
			},
			tg_txt_message:{
				required:"Please enter message."
				}
		},
		
		submitHandler:function(form)
		{
			form.submit();
		}
	});	
});