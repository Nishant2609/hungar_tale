//http://chartjs.devexpress.com/Documentation/ApiReference/dxCircularGauge/Methods#optionoptions
		$(function ()  
			{
			   $('#chartContainer').dxCircularGauge({
				scale: {
					startValue: 0,
					endValue: 15,
					majorTick: {
						tickInterval: 5
					},
					 label:false
				},
				// rangeContainer: {
					// backgroundColor: '#727272',
					// width:30,
            		// offset: 15,
					// ranges: [
						// { startValue: -10, endValue: 0, color: 'green'  },
						// { startValue: 0, endValue: 10, color: 'red' },
						// { startValue: 10, endValue: 20, color: 'yellow' },
					// ]
				// },
				title: {
					text: '',
					font: { size: 24 }
				},
				value: 15,
				size: {
					height: 240,
					width: 280
				},
				
				
				//subtitle: '(Today voting)',
				//type: 'triangleMarker',
				 /*subvalueIndicator: {
type: 'textcloud',
color: 'darkturquoise',
text: {
customizeText: function (gaugeValue) {
return gaugeValue.valueText + ' mph'
}
}
},
				 subvalues: [-5, 5,15],*/
				  tooltip: {
					enabled: true,
					//format: 'currency',
					//precision: 2,
					//customizeText: function (valueIndicator) {
					//	return "Theft 0 to 10";
					//},
					font: {
						size: 20,
						weight: 700
					}
				},
			
			  value: $('#weight1').val(),
			   
			  valueIndicator: {
					type: 'twoColorNeedle',
					color: 'red',
					secondColor: 'skyblue',
					width: 5,
					
					//offset: 70,
					
					
					spindleSize: 30,
					spindleGapSize: 15
				},
				containerBackgroundColor: 'red',
				 geometry: {
					startAngle:135,
					endAngle: 360
				}
			});
			
			
			
			$('#chartContainer2').dxCircularGauge({
				scale: {
					startValue: 0,
					endValue: 15,
					majorTick: {
						tickInterval:5
					},
					label:false
				},
				// rangeContainer: {
					// backgroundColor: '#727272',
					// width:30,
            		// offset: 15,
					// ranges: [
						// { startValue: -10, endValue: 0, color: 'green'  },
						// { startValue: 0, endValue: 10, color: 'red' },
						// { startValue: 10, endValue: 20, color: 'yellow' },
					// ]
				// },
				title: {
					text: '',
					font: { size: 24 }
				},
				//value: 15,
				size: {
					height: 240,
					width: 280
				},
				//subtitle: '(Today voting)',
				/*subvalueIndicator: {
					type: 'triangleMarker',
					color: 'forestgreen',
					length: 20
		 
				},
				 subvalues: [0, 10],*/
				  tooltip: {
					enabled: true,
					//format: 'currency',
					//precision: 2,
					//customizeText: function (valueIndicator) {
					//	return "Theft 0 to 10";
					//},
					font: {
						size: 20,
						weight: 700
					}
				},
			  
			  value: $('#avginput').val(),
			  
			  valueIndicator: {
					type: 'twoColorNeedle',
					color: 'red',
					secondColor: 'skyblue',
					width: 5,
					
					//offset: 70,
					
					
					spindleSize: 30,
					spindleGapSize: 15
				},
				containerBackgroundColor: 'red',
				 geometry: {
					startAngle:135,
					endAngle: 360
				}
			});
			
			
			$('#weight').click(function () {
				var gauge = $('#chartContainer').dxCircularGauge('instance');
				//var val = $('#weight1').val();
				var val =$('#weight1').val();
				
		        gauge.value(val);
			});
			
			$('#submit').click(function () {
				var gauge = $('#chartContainer2').dxCircularGauge('instance');
				var val = $('#avginput').val();
			    gauge.value(val);
			});
			
			
			
			
			
			});
		
		
		
		
		
		$(function ()  
			{
			   $('#chartContainer1').dxCircularGauge({
				scale: {
					startValue: 0,
					endValue: 15,
					majorTick: {
						tickInterval: 5
					},
					 label:false
				},
				// rangeContainer: {
					// backgroundColor: '#727272',
					// width:30,
            		// offset: 15,
					// ranges: [
						// { startValue: -10, endValue: 0, color: 'green'  },
						// { startValue: 0, endValue: 10, color: 'red' },
						// { startValue: 10, endValue: 20, color: 'yellow' },
					// ]
				// },
				title: {
					text: '',
					font: { size: 24 }
				},
				value: 15,
				size: {
					height: 120,
					width: 160
				},
				
				
				//subtitle: '(Today voting)',
				//type: 'triangleMarker',
				 /*subvalueIndicator: {
type: 'textcloud',
color: 'darkturquoise',
text: {
customizeText: function (gaugeValue) {
return gaugeValue.valueText + ' mph'
}
}
},
				 subvalues: [-5, 5,15],*/
				  tooltip: {
					enabled: true,
					//format: 'currency',
					//precision: 2,
					//customizeText: function (valueIndicator) {
					//	return "Theft 0 to 10";
					//},
					font: {
						size: 20,
						weight: 700
					}
				},
			
			  value: $('#weight11').val(),
			   
			  valueIndicator: {
					type: 'twoColorNeedle',
					color: 'red',
					secondColor: 'skyblue',
					width: 5,
					
					//offset: 70,
					
					
					spindleSize: 30,
					spindleGapSize: 15
				},
				containerBackgroundColor: 'red',
				 geometry: {
					startAngle:135,
					endAngle: 360
				}
			});
			
			
			
			$('#chartContainer21').dxCircularGauge({
				scale: {
					startValue: 0,
					endValue: 15,
					majorTick: {
						tickInterval:5
					},
					label:false
				},
				// rangeContainer: {
					// backgroundColor: '#727272',
					// width:30,
            		// offset: 15,
					// ranges: [
						// { startValue: -10, endValue: 0, color: 'green'  },
						// { startValue: 0, endValue: 10, color: 'red' },
						// { startValue: 10, endValue: 20, color: 'yellow' },
					// ]
				// },
				title: {
					text: '',
					font: { size: 24 }
				},
				//value: 15,
				size: {
					height: 130,
					width: 160
				},
				//subtitle: '(Today voting)',
				/*subvalueIndicator: {
					type: 'triangleMarker',
					color: 'forestgreen',
					length: 20
		 
				},
				 subvalues: [0, 10],*/
				  tooltip: {
					enabled: true,
					//format: 'currency',
					//precision: 2,
					//customizeText: function (valueIndicator) {
					//	return "Theft 0 to 10";
					//},
					font: {
						size: 20,
						weight: 700
					}
				},
			  
			  value: $('#avginput1').val(),
			  
			  valueIndicator: {
					type: 'twoColorNeedle',
					color: 'red',
					secondColor: 'skyblue',
					width: 5,
					
					//offset: 70,
					
					
					spindleSize: 30,
					spindleGapSize: 15
				},
				containerBackgroundColor: 'red',
				 geometry: {
					startAngle:135,
					endAngle: 360
				}
			});
			
			
			$('#weight').click(function () {
				var gauge = $('#chartContainer1').dxCircularGauge('instance');
				//var val = $('#weight1').val();
				var val =$('#weight11').val();
				
		        gauge.value(val);
			});
			
			$('#submit').click(function () {
				var gauge = $('#chartContainer21').dxCircularGauge('instance');
				var val = $('#avginput1').val();
			    gauge.value(val);
			});
			
			
			
			
			
			});
		
		
		$(function ()  
			{
			   $('#chartContainer3').dxCircularGauge({
				scale: {
					startValue: 0,
					endValue: 15,
					majorTick: {
						tickInterval: 5
					},
					 label:false
				},
				// rangeContainer: {
					// backgroundColor: '#727272',
					// width:30,
            		// offset: 15,
					// ranges: [
						// { startValue: -10, endValue: 0, color: 'green'  },
						// { startValue: 0, endValue: 10, color: 'red' },
						// { startValue: 10, endValue: 20, color: 'yellow' },
					// ]
				// },
				title: {
					text: '',
					font: { size: 24 }
				},
				value: 15,
				size: {
					height: 240,
					width: 280
				},
				
				
				//subtitle: '(Today voting)',
				//type: 'triangleMarker',
				 /*subvalueIndicator: {
type: 'textcloud',
color: 'darkturquoise',
text: {
customizeText: function (gaugeValue) {
return gaugeValue.valueText + ' mph'
}
}
},
				 subvalues: [-5, 5,15],*/
				  tooltip: {
					enabled: true,
					//format: 'currency',
					//precision: 2,
					//customizeText: function (valueIndicator) {
					//	return "Theft 0 to 10";
					//},
					font: {
						size: 20,
						weight: 700
					}
				},
			
			  value: $('#weight13').val(),
			   
			  valueIndicator: {
					type: 'twoColorNeedle',
					color: 'red',
					secondColor: 'skyblue',
					width: 5,
					
					//offset: 70,
					
					
					spindleSize: 30,
					spindleGapSize: 15
				},
				containerBackgroundColor: 'red',
				 geometry: {
					startAngle:135,
					endAngle: 360
				}
			});
			
			
			
			$('#chartContainer23').dxCircularGauge({
				scale: {
					startValue: 0,
					endValue: 15,
					majorTick: {
						tickInterval:5
					},
					label:false
				},
				// rangeContainer: {
					// backgroundColor: '#727272',
					// width:30,
            		// offset: 15,
					// ranges: [
						// { startValue: -10, endValue: 0, color: 'green'  },
						// { startValue: 0, endValue: 10, color: 'red' },
						// { startValue: 10, endValue: 20, color: 'yellow' },
					// ]
				// },
				title: {
					text: '',
					font: { size: 24 }
				},
				//value: 15,
				size: {
					height: 240,
					width: 280
				},
				//subtitle: '(Today voting)',
				/*subvalueIndicator: {
					type: 'triangleMarker',
					color: 'forestgreen',
					length: 20
		 
				},
				 subvalues: [0, 10],*/
				  tooltip: {
					enabled: true,
					//format: 'currency',
					//precision: 2,
					//customizeText: function (valueIndicator) {
					//	return "Theft 0 to 10";
					//},
					font: {
						size: 20,
						weight: 700
					}
				},
			  
			  value: $('#avginput3').val(),
			  
			  valueIndicator: {
					type: 'twoColorNeedle',
					color: 'red',
					secondColor: 'skyblue',
					width: 5,
					
					//offset: 70,
					
					
					spindleSize: 30,
					spindleGapSize: 15
				},
				containerBackgroundColor: 'red',
				 geometry: {
					startAngle:135,
					endAngle: 360
				}
			});
			
			
			$('#weight').click(function () {
				var gauge = $('#chartContainer3').dxCircularGauge('instance');
				//var val = $('#weight1').val();
				var val =$('#weight13').val();
				
		        gauge.value(val);
			});
			
			$('#submit').click(function () {
				var gauge = $('#chartContainer23').dxCircularGauge('instance');
				var val = $('#avginput3').val();
			    gauge.value(val);
			});
			
			
			
			
			
			});