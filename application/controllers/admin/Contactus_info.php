<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus_info extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Contactus_info_model');
		          $this->load->helper(array('form', 'url'));
	   }
	   
	public function index()
	{
			$data['role'] = $this->Contactus_info_model->show_roles();
			$this->load->view('admin/Contactus_info_view',$data);		
	}
	
	public function ajax_list()
    {
  		$list = $this->Contactus_info_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $Contactus_info) {
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$Contactus_info->contactus_info_id.'">';
			$row[] = $no;
            $row[] = $Contactus_info->contactus_info_address;
			$row[] = $Contactus_info->contactus_info_phone1;
			$row[] = $Contactus_info->contactus_info_phone2;
			$row[] = $Contactus_info->contactus_info_phone3;
			$row[] = $Contactus_info->contactus_info_email;
			
			$status_class = ($Contactus_info->status?"btn-success":"btn-danger"); // returns true
			$status =($Contactus_info->status? "Active" : "Inactive");
			$row[] = '<i data='."'".$Contactus_info->contactus_info_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$Contactus_info->contactus_info_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$Contactus_info->contactus_info_id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Contactus_info_model->count_all(),
                        "recordsFiltered" => $this->Contactus_info_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	
	public function ajax_edit($id)
    {
        $data = $this->Contactus_info_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
		 $data = array(
                'contactus_info_address' => $this->input->post('contactusAddress'),
                'contactus_info_phone1' => $this->input->post('contactusPhone1'),
				'contactus_info_phone2' => $this->input->post('contactusPhone2'),
				'contactus_info_phone3' => $this->input->post('contactusPhone3'),
                'contactus_info_email' => $this->input->post('contactusEmail'),
				'added_date' => date("Y-m-d H:i:s"),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		
		$insert = $this->Contactus_info_model->save($data);
       echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		$data = array(
               'contactus_info_address' => $this->input->post('contactusAddress'),
                'contactus_info_phone1' => $this->input->post('contactusPhone1'),
                'contactus_info_phone2' => $this->input->post('contactusPhone2'),
                'contactus_info_phone3' => $this->input->post('contactusPhone3'),
                'contactus_info_email' => $this->input->post('contactusEmail'),
                'modified_date' => date("Y-m-d H:i:s"),
                'status'=>'1'
            );
			
		
		$this->Contactus_info_model->update(array('contactus_info_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->Contactus_info_model->get_by_id($id);
        
		$this->Contactus_info_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Contactus_info_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
	
	 private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('contactusAddress') == '')
        {
            $data['inputerror'][] = 'contactusAddress';
            $data['error_string'][] = 'Address is required';
            $data['status'] = FALSE;
        }
		if($this->input->post('contactusPhone1') == '')
        {
            $data['inputerror'][] = 'contactusPhone1';
            $data['error_string'][] = 'Phone1 is required';
            $data['status'] = FALSE;
        }
		/*else if(strlen($this->input->post('contactusPhone1'))>10 || strlen($this->input->post('contactusPhone1'))<10)
        {
            $data['inputerror'][] = 'contactusPhone1';
            $data['error_string'][] = '10 digit are required';
            $data['status'] = FALSE;
        }
		else
		{
		}*/
       if($this->input->post('contactusEmail') == '')
        {
            $data['inputerror'][] = 'contactusEmail';
            $data['error_string'][] = 'Email is required';
            $data['status'] = FALSE;
        }
		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	 public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('contactus_info_id');
		        $this->Contactus_info_model->update_status($c_id,$status);
	}

}
?>
