<?php
class Contactus extends CI_Controller 
{

	function __construct() 
	{
		parent::__construct();
		$this->load->model('admin/Contactus_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library("pagination");
	}
	
	function index() 
	{
		if($this->session->userdata('empid')!='')
	  	 {
			//load view
			$this->load->view('admin/Contactus_view');
		 }
		 else
		 {
			 redirect('admin/Login');
		 } 
	}
	
	public function ajax_list()
    {
        //get content
		$list = $this->Contactus_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $subject) {
		
            $no++;
            $row = array();
            $row[] = '<input type="checkbox"  name="forms[]" id="forms[]" class="checkbox"
                   value='."'".$subject->contactus_id."'".'/>';
            $row[] = $subject->contact_name;
			$row[] = $subject->contact_email;
			$row[] = $subject->contact_message;
			//display the status on button		   
  			$row[] = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" contact_name="Delete" onclick="delete_subject('."'".$subject->contactus_id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
			
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => intval($_POST['draw']),
                        "recordsTotal" => $this->Contactus_model->count_all(),
                        "recordsFiltered" => $this->Contactus_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
//delete the details
    public function ajax_delete($contactus_id)
    {
	    $data = array( 

						'is_deleted' => 1

						); 
        $this->Contactus_model->delete_by_id($contactus_id,$data);
        echo json_encode(array("status" => TRUE));
    }
 	function delete_checkbox() 
	 {
			//delete multiple countries selected in checkbox
   			$dat = $this->input->post('forms');
			if(sizeof($dat)==" ")
			{
			$this->session->set_flashdata('deletemsgs', 'Please select atleast one record');
			}
			else
			{
					$data = array( 
							'is_deleted' => 1
							);
					 for ($i = 0; $i < sizeof($dat); $i++) 
					 {
						$this->Contactus_model->delete_check($dat[$i],$data);
					 }
					 $this->session->set_flashdata('deletemsg', 'Your data deleted successfully');
			}
			redirect('admin/Contactus/');
				
		}
}
?>
