<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Food_type extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Food_type_model');
                  $this->load->helper(array('form', 'url','file'));
                  $this->load->library('session');
                 
	   }
	   
	public function index()
	{
        if(!$this->session->has_userdata('empid')){
            $this->load->view('admin/Login_view');
            
        }else {
			$data['role'] = $this->Food_type_model->show_roles();
            $this->load->view('admin/Food_type_view',$data);
        }		
	}
	
	public function ajax_list()
    {
  		$list = $this->Food_type_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $User) {
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$User->type_id.'">';
            
			$row[] = $User->type_name;                        
            $row[] =($User->is_popular? "Yes" : "No");
            
            $status_class = ($User->is_active?"btn-success":"btn-danger"); // returns true
			$status =($User->is_active? "Active" : "Inactive");
			$row[] = '<i data='."'".$User->type_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$User->type_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$User->type_id."'".')"><i class="glyphicon glyphicon-trash"></i> </a>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Food_type_model->count_all(),
                        "recordsFiltered" => $this->Food_type_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	
	public function ajax_edit($id)
    {
        $data = $this->Food_type_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
         if($this->input->post('is_popular')!='')
				{
					$status=1;
				}
				else
				{
					$status=0;
                }
		 $data = array(
                
                'type_name' => $this->input->post('food_type'),
                
                'is_popular' => $status
				
            );
		
		
		
        $insert = $this->Food_type_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		if($this->input->post('home_status')!='')
				{
					$status=1;
				}
				else
				{
					$status=0;
                }
		 $data = array(
                
            'type_name' => $this->input->post('food_type'),
                
            'is_popular' => $status
				
            );
			
		
		
        $this->Slider_model->update(array('type_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        //delete file
        //$person = $this->Food_type_model->get_by_id($id);
       
		$this->Food_type_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Food_type_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
	
	 
	
	 private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('food_type') == '')
        {
            $data['inputerror'][] = 'food_type';
            $data['error_string'][] = 'Food type name is required';
            $data['status'] = FALSE;
        }
		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	 public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('home_status');
				$c_id = $this->input->post('type_id');
		        $this->Food_type_model->update_status($c_id,$status);
	}

}
?>