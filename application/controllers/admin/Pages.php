<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Pages_model');
		          $this->load->helper(array('form', 'url'));
	   }
	   
	public function index()
	{
			$data['role'] = $this->Pages_model->show_roles();
			$this->load->view('admin/Pages_view',$data);		
	}
	
	public function ajax_list()
    {
  		$list = $this->Pages_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $Page) {
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$Page->page_id.'">';
            $row[] = $Page->page_title;
			$row[] = $Page->page_content;
          	if($Page->page_image)
                $row[] = '<a href="'.base_url('upload/page/'.$Page->page_image).'" target="_blank"><img src="'.base_url('upload/page/'.$Page->page_image).'" class="img-responsive" width="50" height="50" /></a>';
            else
                $row[] = '(No photo)';
			
			$status_class = ($Page->status?"btn-success":"btn-danger"); // returns true
			$status =($Page->status? "Active" : "Inactive");
			$row[] = '<i data='."'".$Page->page_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" page_title="Edit" onclick="edit_user('."'".$Page->page_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" page_title="Delete" onclick="delete_user('."'".$Page->page_id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pages_model->count_all(),
                        "recordsFiltered" => $this->Pages_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	
	public function ajax_edit($id)
    {
        $data = $this->Pages_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
		 $data = array(
                'page_title' => $this->input->post('pageTitle'),
                'page_content' => $this->input->post('pageContent'),
				'added_date' => date("Y-m-d H:i:s"),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		
		if(!empty($_FILES['page_image']['name']))
        {
            $upload = $this->_do_upload();
            $data['page_image'] = $upload;
        }
		
		
        $insert = $this->Pages_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		$data = array(
                'page_title' => $this->input->post('pageTitle'),
                'page_content' => $this->input->post('pageContent'),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
			
		if($this->input->post('remove_photo')) // if remove photo checked
        {
            if(file_exists('upload/page/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
                unlink('upload/page/'.$this->input->post('remove_photo'));
            $data['page_image'] = '';
        }
 
        if(!empty($_FILES['page_image']['name']))
        {
            $upload = $this->_do_upload();
             
            //delete file
            $person = $this->Pages_model->get_by_id($this->input->post('id'));
            if(file_exists('upload/page/'.$person->page_image) && $person->page_image)
                unlink('upload/page/'.$person->page_image);
 
            $data['page_image'] = $upload;
        }
		$this->Pages_model->update(array('page_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->Pages_model->get_by_id($id);
        if(file_exists('upload/page/'.$person->page_image) && $person->page_image)
        unlink('upload/page/'.$person->page_image);
		$this->Pages_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Pages_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
	 private function _do_upload()
    {
        $config['upload_path']          = 'upload/page/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['create_thumb'] 		= TRUE;
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 		
        //$this->load->library('upload', $config);
 		$this->upload->initialize($config);
        if(!$this->upload->do_upload('page_image')) //upload and validate
        {
            $data['inputerror'][] = 'page_image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }
	private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('pageTitle') == '')
        {
            $data['inputerror'][] = 'pageTitle';
            $data['error_string'][] = 'slider page_title is required';
            $data['status'] = FALSE;
        }
		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	 public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('page_id');
		        $this->Pages_model->update_status($c_id,$status);
	}

}
?>
