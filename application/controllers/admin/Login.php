<?php
	class Login extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('admin/Login_model');
		}
		
		function index()
		{
			$this->load->view('admin/Login_view');
        }
			
		function login()
		{
			$email= $this->input->post('email');
		    $password= $this->input->post('password');
			
			$result=$this->Login_model->login_user($email,$password);
			
			if($result)
			{
				$this->show_message($email,$password);
				
			}
			else
			{
				$this->show_error_message();
			}
		}
		
		function show_message($email,$password) 
		{
			$this->load->library('session');
            $ip_address= $_SERVER['REMOTE_ADDR']; 
			$user_agent=$_SERVER['HTTP_USER_AGENT'];
			$titme=time();
			
			$session_data = array(
									 'session_id'    => md5(rand(10,500)),
									 'ip_address'    => $ip_address,
									 'user_agent'    => $user_agent,
									 'last_activity' => $titme
							      );
								  
			$result=$this->Login_model->inser_session_data($session_data);
			$this->session->set_userdata('email',$email);
			$this->session->set_userdata('password',$password);
			$data=$this->Login_model->show_user_id($email,$password);
			foreach($data as $user)
			{
					  $user_id=$user->user_id;
					  $name=$user->first_name;
			}
			
			$this->session->set_userdata('empid',$user_id);
			$this->session->set_userdata('name',$name);
			redirect('admin/Login/welcome');
		}
		function show_error_message() 
		{
			$data['error_message'] = 'Invalid Email Id and password, please try again.';
			$this->load->view('admin/Login_view', $data);
		}
		function logout()
		{
			//load view
					$this->session->unset_userdata('empid');
					$this->session->unset_userdata('name');
					$this->session->unset_userdata('email');
					$this->session->unset_userdata('password');
					$this->session->sess_destroy();
					$this->load->view('admin/Login_view');
			
			
		}
		
		function welcome()
		{ 
				 $this->load->view('admin/Welcome_message');
		}
		
		
		function profile()
		{ 
				$uid=$this->session->userdata('empid');
				$data['records'] = $this->Login_model->getemployeedetails($uid);
				$this->load->view('admin/Profile',$data);
		}
		
	  public function profile_edit_view() 
	   { 
	     
			 //Display view of profile for perticular employee from tg_employee table
			 $uid=$this->session->userdata('empid');
			 $data['records'] = $this->Login_model->getemployeedetails($uid);
			 $this->load->view('admin/Profile_edit_view',$data);
      }
	  
	  public function update_profile()
	  { 
	   		//edit the profile for perticular employee from tg_employee table
	  		$id=$this->input->post('id'); 
			if(isset($_FILES['image']['name']) && $_FILES['image']['name']!='')
			{ 	
					$image=$_FILES['image']['name'];
			}
			 else
		 	{
				 	$image=$this->input->post('image');
		 	}	
        	 $data=array(        
            					'first_name' => $this->input->post('firstname'),
								'last_name' => $this->input->post('lastname'),
								'email' => $this->input->post('email'),
								'password' => $this->input->post('password'),
								'mobile' => $this->input->post('mobile'),	
								'image' => $image
         				); 
			$this->Login_model->update($data,$id); 		 
			$config['upload_path']    = 'upload';
			$config['allowed_types']  = 'gif|jpg|png';
			$file_name                = $image;
			$config['file_name']      = $file_name;
			$this->upload->initialize($config);
				if (!is_dir('upload'))
				{
					 mkdir('./upload/', 0777, true);
				}		 
				if ( ! $this->upload->do_upload('image'))
				{
					//error in file uploading
					$data['message'] = $this->upload->display_errors();
				} 
			 $this->session->set_flashdata('editmsg', 'Your data updated successfully');
			 redirect('admin/Login/profile'); 
      } 
	  
	  public function update_image()
		{
				//update the edited image
				$id = $this->input->get('id');
				$image = $this->input->get('img');		
				if(@unlink("./upload/".$image))
				{
					$this->Login_model->update_image_id($id);
					$data['records'] = $this->Login_model->show_user($id);
					$this->load->view('admin/Profile_edit_view',$data);
				}
		}	 
	  
	  
	}
?>
