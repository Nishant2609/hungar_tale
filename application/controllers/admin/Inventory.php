<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Inventory extends CI_Controller 
{
	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Inventory_model');
		          $this->load->helper(array('form', 'url'));
	   }
	public function index()
	{
			$data['role'] = $this->Inventory_model->show_roles();
			$this->load->view('admin/Inventory_view',$data);		
	}
	public function ajax_list()
    {
  		$list = $this->Inventory_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $Invent) {
		$view = base_url()."admin/inventory/view_details/".$Invent->item_id;
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$Invent->item_id.'">';
			$row[] = $no;
            $row[] = '<a href='."'".$view."'".' data-fancybox data-type="iframe" title="View Details" style="text-align: center;">'.$Invent->item_name.'</a>';
			$row[] = substr($Invent->item_description,0,200).' ...';
			$row[] = $Invent->item_price;
			$row[] = $Invent->item_tax;
			$row[] = $Invent->item_quantity;
			$row[] = $Invent->added_date;
			$row[] = $Invent->modified_date;
			$status_class = ($Invent->status?"btn-success":"btn-danger"); // returns true
			$status =($Invent->status? "Active" : "Inactive");
			$row[] = '<i data='."'".$Invent->item_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$Invent->item_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$Invent->item_id."'".')"><i class="glyphicon glyphicon-trash"></i> </a>';
            $data[] = $row;
        }
	$output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Inventory_model->count_all(),
                        "recordsFiltered" => $this->Inventory_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	public function ajax_edit($id)
    {
        $data = $this->Inventory_model->get_by_id($id);
        echo json_encode($data);
    }
    public function ajax_add()
    {
         $this->_validate();
		 $data = array(
                'item_name' => $this->input->post('itemName'),
                'item_description' => $this->input->post('itemDescription'),
				'item_price' => $this->input->post('itemPrice'),
				'item_tax' => $this->input->post('itemTax'),
				'item_quantity' => $this->input->post('itemQuantity'),
				'added_date' => date("Y-m-d H:i:s"),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		$insert = $this->Inventory_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
    public function ajax_update()
    {
        $this->_validate();
		$data = array(
                'item_name' => $this->input->post('itemName'),
                'item_description' => $this->input->post('itemDescription'),
				'item_price' => $this->input->post('itemPrice'),
				'item_tax' => $this->input->post('itemTax'),
				'item_quantity' => $this->input->post('itemQuantity'),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		$this->Inventory_model->update(array('item_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->Inventory_model->get_by_id($id);
       	$this->Inventory_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
    public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Inventory_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('itemName') == '')
        {
            $data['inputerror'][] = 'itemName';
            $data['error_string'][] = 'item name is required';
            $data['status'] = FALSE;
        }
		if($this->input->post('itemPrice') == '')
        {
            $data['inputerror'][] = 'itemPrice';
            $data['error_string'][] = 'item price is required';
            $data['status'] = FALSE;
        }
		if($this->input->post('itemTax') == '')
        {
            $data['inputerror'][] = 'itemTax';
            $data['error_string'][] = 'item tax is required';
            $data['status'] = FALSE;
        }
		if($this->input->post('itemQuantity') == '')
        {
            $data['inputerror'][] = 'itemQuantity';
            $data['error_string'][] = 'item quantity is required';
            $data['status'] = FALSE;
        }
		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('item_id');
		        $this->Inventory_model->update_status($c_id,$status);
	}
	public function view_details($item_id)
	{
	
		$data['item_details']=$this->Inventory_model->get_item_details($item_id);
		$this->load->view('admin/Item_details_view',$data);
	}

}
?>