<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Social_model');
		          $this->load->helper(array('form', 'url'));
	   }
	   
	public function index()
	{
			$data['role'] = $this->Social_model->show_roles();
			$this->load->view('admin/Social_view',$data);		
	}
	
	public function ajax_list()
    {
  		$list = $this->Social_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $User) {
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$User->social_id.'">';
            $row[] = $User->social_name;
			$row[] = $User->social_link;
          	$status_class = ($User->status?"btn-success":"btn-danger"); // returns true
			$status =($User->status? "Active" : "Inactive");
			$row[] = '<i data='."'".$User->social_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$User->social_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <!--<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$User->social_id."'".')"><i class="glyphicon glyphicon-trash"></i> </a>-->';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Social_model->count_all(),
                        "recordsFiltered" => $this->Social_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	
	public function ajax_edit($id)
    {
        $data = $this->Social_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
		 $data = array(
                'social_name' => $this->input->post('socialName'),
                'social_link' => $this->input->post('socialLink'),
				'added_date' => date("Y-m-d H:i:s"),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		$insert = $this->Social_model->save($data);
       echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		$data = array(
                'social_name' => $this->input->post('socialName'),
                'social_link' => $this->input->post('socialLink'),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		$this->Social_model->update(array('social_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        $this->Social_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Social_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('socialName') == '')
        {
            $data['inputerror'][] = 'socialName';
            $data['error_string'][] = 'Social Media title is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('socialLink') == '')
        {
            $data['inputerror'][] = 'socialLink';
            $data['error_string'][] = 'Social Media link is required';
            $data['status'] = FALSE;
        }

		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	 public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('social_id');
		        $this->Social_model->update_status($c_id,$status);
	}

}
?>
