<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Services extends CI_Controller 
{
	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Services_model');
		          $this->load->helper(array('form', 'url'));
	   }
	public function index()
	{
			$data['role'] = $this->Services_model->show_roles();
			$this->load->view('admin/Services_view',$data);		
	}
	public function ajax_list()
    {
  		$list = $this->Services_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $Service) {
		$view = base_url()."admin/services/view_details/".$Service->service_id;
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$Service->service_id.'">';
			$row[] = $no;
            $row[] = '<a href='."'".$view."'".' data-fancybox data-type="iframe" title="View Details" style="text-align: center;">'.$Service->service_name.'</a>';
			$row[] = substr($Service->service_description,0,200).' ...';
			if($Service->service_image)
                $row[] = '<a href="'.base_url('upload/service/'.$Service->service_image).'" target="_blank"><img src="'.base_url('upload/service/'.$Service->service_image).'" class="img-responsive" width="50" height="50" /></a>';
            else
                $row[] = '(No photo)';
			$status_class = ($Service->status?"btn-success":"btn-danger"); // returns true
			$status =($Service->status? "Active" : "Inactive");
			$row[] = '<i data='."'".$Service->service_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$Service->service_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$Service->service_id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
            $data[] = $row;
        }
	$output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Services_model->count_all(),
                        "recordsFiltered" => $this->Services_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	public function ajax_edit($id)
    {
        $data = $this->Services_model->get_by_id($id);
        echo json_encode($data);
    }
    public function ajax_add()
    {
         $this->_validate();
		 $data = array(
                'service_name' => $this->input->post('serviceName'),
                'service_description' => $this->input->post('serviceDescription'),
				'added_date' => date("Y-m-d H:i:s"),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		
		if(!empty($_FILES['service_image']['name']))
        {
            $upload = $this->_do_upload();
            $data['service_image'] = $upload;
        }
		$insert = $this->Services_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
    public function ajax_update()
    {
        $this->_validate();
		$data = array(
                'service_name' => $this->input->post('serviceName'),
                'service_description' => $this->input->post('serviceDescription'),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
			
		if($this->input->post('remove_photo')) // if remove photo checked
        {
            if(file_exists('upload/service/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
                unlink('upload/service/'.$this->input->post('remove_photo'));
            $data['service_image'] = '';
        }
       if(!empty($_FILES['service_image']['name']))
        {
            $upload = $this->_do_upload();
             
            //delete file
            $person = $this->Services_model->get_by_id($this->input->post('id'));
            if(file_exists('upload/service/'.$person->service_image) && $person->service_image)
                unlink('upload/service/'.$person->service_image);
 
            $data['service_image'] = $upload;
        }
		$this->Services_model->update(array('service_id' => $this->input->post('id')), $data);
       echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->Services_model->get_by_id($id);
        if(file_exists('upload/service/'.$person->service_image) && $person->service_image)
        unlink('upload/service/'.$person->service_image);
		$this->Services_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
    public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Services_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	private function _do_upload()
    {
        $config['upload_path']          = 'upload/service/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['create_thumb'] 		= TRUE;
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 		
        //$this->load->library('upload', $config);
 		$this->upload->initialize($config);
        if(!$this->upload->do_upload('service_image')) //upload and validate
        {
            $data['inputerror'][] = 'service_image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }
	private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('serviceName') == '')
        {
            $data['inputerror'][] = 'serviceName';
            $data['error_string'][] = 'service name is required';
            $data['status'] = FALSE;
        }
		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('service_id');
		        $this->Services_model->update_status($c_id,$status);
	}
	public function view_details($service_id)
	{
	
		$data['service_details']=$this->Services_model->get_service_details($service_id);
		$this->load->view('admin/Service_details_view',$data);
	}

}
?>