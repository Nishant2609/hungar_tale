<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Slider_model');
		          $this->load->helper(array('form', 'url','file'));
	   }
	   
	public function index()
	{
			$data['role'] = $this->Slider_model->show_roles();
			$this->load->view('admin/Slider_view',$data);		
	}
	
	public function ajax_list()
    {
  		$list = $this->Slider_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $User) {
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$User->img_id.'">';
            //$row[] = $User->image_name;
			$row[] = $User->image_title;
          	if($User->image_name)
                $row[] = '<a href="'.base_url('uploads/slider_files/'.$User->image_name).'" target="_blank"><img src="'.base_url('uploads/slider_files/'.$User->image_name).'" class="img-responsive" width="50" height="50" /></a>';
            else
                $row[] = '(No photo)';
                
                $row[] =($User->is_home? "Yes" : "No");
            
            $status_class = ($User->is_active?"btn-success":"btn-danger"); // returns true
			$status =($User->is_active? "Active" : "Inactive");
			$row[] = '<i data='."'".$User->img_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$User->img_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$User->img_id."'".')"><i class="glyphicon glyphicon-trash"></i> </a>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Slider_model->count_all(),
                        "recordsFiltered" => $this->Slider_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	
	public function ajax_edit($id)
    {
        $data = $this->Slider_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
         if($this->input->post('home_status')!='')
				{
					$status=1;
				}
				else
				{
					$status=0;
                }
		 $data = array(
                
                'image_title' => $this->input->post('sliderDescription'),
                
                'is_home' => $status
				
            );
		
		if(!empty($_FILES['image']['name']))
        {
            $upload = $this->_do_upload();
            $data['image_name'] = $upload;
            $data['image_url'] = base_url().'uploads/slider_files/'.$data['image_name'];
           // print_r($data);die;
        }
		
		
        $insert = $this->Slider_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		if($this->input->post('home_status')!='')
				{
					$status=1;
				}
				else
				{
					$status=0;
                }
		 $data = array(
                
                'image_title' => $this->input->post('sliderDescription'),
                
                'is_home' => $status
				
            );
			
		if($this->input->post('remove_photo')) // if remove photo checked
        {
            if(file_exists('uploads/slider_files/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
                unlink('uploads/slider_files/'.$this->input->post('remove_photo'));
            $data['image_name'] = '';
        }
 
        if(!empty($_FILES['image']['name']))
        {
            $upload = $this->_do_upload();
           
            //delete file
            $person = $this->Slider_model->get_by_id($this->input->post('id'));
            if(file_exists('uploads/slider_files/'.$person->image_name) && $person->image_name)
                unlink('uploads/slider_files/'.$person->image_name);
        
            $data['image_name'] = $upload;
            $data['image_url'] = base_url().'uploads/slider_files/'.$data['image_name'];
            
            /* $data['image_url'] = $upload; */
        }
		
        $this->Slider_model->update(array('img_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->Slider_model->get_by_id($id);
        if(file_exists('uploads/slider_files/'.$person->image_name) && $person->image_name)
        unlink('uploads/slider_files/'.$person->image_name);
		$this->Slider_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Slider_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
	 private function _do_upload()
    {
        $config['upload_path']          = 'uploads/slider_files/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['create_thumb'] 		= TRUE;
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
        
        $this->load->library('upload', $config);
         //$this->upload->initialize($config);
         
        if(!$this->upload->do_upload('image')) //upload and validate
        {
            $data['inputerror'][] = 'image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }
	 
	
	 private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('sliderDescription') == '')
        {
            $data['inputerror'][] = 'sliderTitle';
            $data['error_string'][] = 'Slider title is required';
            $data['status'] = FALSE;
        }
		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	 public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('slider_id');
		        $this->Slider_model->update_status($c_id,$status);
	}

}
?>