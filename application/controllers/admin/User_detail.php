<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_detail extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/User_detail_model');
		          $this->load->helper(array('form', 'url'));
	   }
	   
	public function index()
	{
			$data['role'] = $this->User_detail_model->show_roles();
			$this->load->view('admin/User_detail_view',$data);		
	}
	
	public function ajax_list()
    {
  		$list = $this->User_detail_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $User) {
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$User->user_id.'">';
            $row[] = $User->first_name;
			$row[] = $User->last_name;
            $row[] = $User->email;
            $row[] = $User->address;
			$row[] = $User->mobile;
			$row[] = $User->role_name;
			if($User->image)
                $row[] = '<a href="'.base_url('upload/'.$User->image).'" target="_blank"><img src="'.base_url('upload/'.$User->image).'" class="img-responsive" width="50" height="50" /></a>';
            else
                $row[] = '(No photo)';
			
			$status_class = ($User->status?"btn-success":"btn-danger"); // returns true
			$status =($User->status? "Active" : "Inactive");
			$row[] = '<i data='."'".$User->user_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$User->user_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$User->user_id."'".')"><i class="glyphicon glyphicon-trash"></i> </a>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->User_detail_model->count_all(),
                        "recordsFiltered" => $this->User_detail_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	
	public function ajax_edit($id)
    {
        $data = $this->User_detail_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
		 $data = array(
                'first_name' => $this->input->post('firstName'),
                'last_name' => $this->input->post('lastName'),
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
                'address' => $this->input->post('address'),
                'mobile' => $this->input->post('mobile'),
				'role_id' => $this->input->post('role')
            );
		
		if(!empty($_FILES['image']['name']))
        {
            $upload = $this->_do_upload();
            $data['image'] = $upload;
        }
		
        $insert = $this->User_detail_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		$data = array(
                'first_name' => $this->input->post('firstName'),
                'last_name' => $this->input->post('lastName'),
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
                'address' => $this->input->post('address'),
                'mobile' => $this->input->post('mobile'),
				'role_id' => $this->input->post('role')
            );
			
		if($this->input->post('remove_photo')) // if remove photo checked
        {
            if(file_exists('upload/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
                unlink('upload/'.$this->input->post('remove_photo'));
            $data['image'] = '';
        }
 
        if(!empty($_FILES['image']['name']))
        {
            $upload = $this->_do_upload();
             
            //delete file
            $person = $this->User_detail_model->get_by_id($this->input->post('id'));
            if(file_exists('upload/'.$person->image) && $person->image)
                unlink('upload/'.$person->image);
 
            $data['image'] = $upload;
        }
			
        $this->User_detail_model->update(array('user_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->User_detail_model->get_by_id($id);
        if(file_exists('upload/'.$person->image) && $person->image)
        unlink('upload/'.$person->image);
		$this->User_detail_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->User_detail_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
	 private function _do_upload()
    {
        $config['upload_path']          = 'upload/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['create_thumb'] 		= TRUE;
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 		
        //$this->load->library('upload', $config);
 		$this->upload->initialize($config);
        if(!$this->upload->do_upload('image')) //upload and validate
        {
            $data['inputerror'][] = 'image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }
	
	 private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('firstName') == '')
        {
            $data['inputerror'][] = 'firstName';
            $data['error_string'][] = 'first name is required';
            $data['status'] = FALSE;
        }
		if($this->input->post('lastName') == '')
        {
            $data['inputerror'][] = 'lastName';
            $data['error_string'][] = 'last name is required';
            $data['status'] = FALSE;
        }

		if($this->input->post('email') == '' )
        {
            $data['inputerror'][] = 'email';
            $data['error_string'][] = 'email is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('password') == '')
        {
            $data['inputerror'][] = 'password';
            $data['error_string'][] = 'password is required';
            $data['status'] = FALSE;
        }

		/*if($this->input->post('address') == '')
        {
            $data['inputerror'][] = 'address';
            $data['error_string'][] = 'address is required';
            $data['status'] = FALSE;
        }*/
		if($this->input->post('mobile') == '')
        {
            $data['inputerror'][] = 'mobile';
            $data['error_string'][] = 'phone number is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('role') == '')
        {
            $data['inputerror'][] = 'role';
            $data['error_string'][] = 'role is required';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	 public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('user_id');
		        $this->User_detail_model->update_status($c_id,$status);
	}

}
?>