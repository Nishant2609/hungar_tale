<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logo extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Logo_model');
		          $this->load->helper(array('form', 'url'));
	   }
	   
	public function index()
	{
			
			$this->load->view('admin/Logo_view');		
	}
	
	public function ajax_list()
    {
  		$list = $this->Logo_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $User) {
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$User->logo_id.'">';
            $row[] = $User->title;
			$row[] = $User->description;
          	if($User->image)
                $row[] = '<a href="'.base_url('upload/logo/'.$User->image).'" target="_blank"><img src="'.base_url('upload/logo/'.$User->image).'" class="img-responsive" width="50" height="50" /></a>';
            else
                $row[] = '(No photo)';
			
			$status_class = ($User->status?"btn-success":"btn-danger"); // returns true
			$status =($User->status? "Active" : "Inactive");
			$row[] = '<i data='."'".$User->logo_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$User->logo_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$User->logo_id."'".')"><i class="glyphicon glyphicon-trash"></i> </a>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Logo_model->count_all(),
                        "recordsFiltered" => $this->Logo_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	
	public function ajax_edit($id)
    {
        $data = $this->Logo_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
		 $data = array(
                'title' => $this->input->post('logoTitle'),
                'description' => $this->input->post('logoDescription'),
				'added_date' => date("Y-m-d H:i:s"),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		
		if(!empty($_FILES['image']['name']))
        {
            $upload = $this->_do_upload();
            $data['image'] = $upload;
        }
		
        $insert = $this->Logo_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		$data = array(
                'title' => $this->input->post('logoTitle'),
                'description' => $this->input->post('logoDescription'),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
			
		if($this->input->post('remove_photo')) // if remove photo checked
        {
            if(file_exists('upload/logo/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
                unlink('upload/logo/'.$this->input->post('remove_photo'));
            $data['image'] = '';
        }
 
        if(!empty($_FILES['image']['name']))
        {
            $upload = $this->_do_upload();
             
            //delete file
            $person = $this->Logo_model->get_by_id($this->input->post('id'));
            if(file_exists('upload/logo/'.$person->image) && $person->image)
                unlink('upload/logo/'.$person->image);
 
            $data['image'] = $upload;
        }
			
        $this->Logo_model->update(array('logo_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->Logo_model->get_by_id($id);
        if(file_exists('upload/logo/'.$person->image) && $person->image)
        unlink('upload/logo/'.$person->image);
		$this->Logo_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Logo_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
	 private function _do_upload()
    {
        $config['upload_path']          = 'upload/logo/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['create_thumb'] 		= TRUE;
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 		
        //$this->load->library('upload', $config);
 		$this->upload->initialize($config);
        if(!$this->upload->do_upload('image')) //upload and validate
        {
            $data['inputerror'][] = 'image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }
	
	 private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('logoTitle') == '')
        {
            $data['inputerror'][] = 'logoTitle';
            $data['error_string'][] = 'logo title is required';
            $data['status'] = FALSE;
        }
		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	 public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('logo_id');
		        $this->Logo_model->update_status($c_id,$status);
	}

}
?>