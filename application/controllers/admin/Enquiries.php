<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiries extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Enquiries_model');
		          $this->load->helper(array('form', 'url'));
	   }
	   
	public function index()
	{
			$data['clients']=$this->Enquiries_model->show_clients();
			$this->load->view('admin/Enquiries_view',$data);		
	}
	
	public function ajax_list()
    {
  		$list = $this->Enquiries_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $details) {
		$view = base_url()."admin/enquiries/view_details/".$details->id;
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$details->id.'">';
            $row[] = $no;
            $row[] = '<a href='."'".$view."'".' data-fancybox data-type="iframe" title="View Details" style="text-align: center;">'.$details->enquiry_generated_id.'</a>';
          	$row[] = $details->enquiry_name;
			$row[] = $details->enquiry_email;
			$row[] = $details->enquiry_mobile;
			$row[] = $details->enquiry_subject;
			if($details->enquiry_type_id==1)
			{
				$row[] ="Service";
			}
			else
			{
				$row[] ="Product";
			}
			$row[] = $details->enquiry_details;		
			$row[] = $details->added_date;
            /*$row[] = $details->modified_date;*/
            
			$status_class = ($details->status?"btn-success":"btn-danger"); // returns true
			$status =($details->status? "Open" : "Closed");
			$row[] = '<i data='."'".$details->id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			/*<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$details->id."'".')"><i class="fa fa-pencil-square-o"></i></a>*/
			$row[] = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$details->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Enquiries_model->count_all(),
                        "recordsFiltered" => $this->Enquiries_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	
	public function ajax_edit($id)
    {
        $data = $this->Enquiries_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
		 date_default_timezone_set('Asia/Kolkata');
         $enquiry_generated_id="PS-".sprintf("%06d", mt_rand(1, 999999));
        
		 $data = array(
                'enquiry_generated_id' => $enquiry_generated_id,
                'coustomer_id_fk'=> $this->input->post('drp_client'),
                'enquiry_subject'=> $this->input->post('txt_enquiry_subject'),
				'enquiry_details'=> $this->input->post('txt_enquiry_details'),
				'added_date' => date('Y-m-d H:i:s'),
				'modified_date' => date('Y-m-d H:i:s')
            );
		
        $insert = $this->Enquiries_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		date_default_timezone_set('Asia/Kolkata');
		$data = array(
				 'coustomer_id_fk'=> $this->input->post('drp_client'),
                 'enquiry_subject'=> $this->input->post('txt_enquiry_subject'),
				'enquiry_details'=> $this->input->post('txt_enquiry_details'),
				 'modified_date' => date('Y-m-d H:i:s')
            );
	
        $this->Enquiries_model->update(array('id' => $this->input->post('id')), $data);
       echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->Enquiries_model->get_by_id($id);
		$this->Enquiries_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Enquiries_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
	
	 private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 		
		if($this->input->post('drp_client') == '')
        {
            $data['inputerror'][] = 'drp_client';
            $data['error_string'][] = 'Please select Coustomer';
            $data['status'] = FALSE;
        }
		
        if($this->input->post('txt_enquiry_subject') == '')
        {
            $data['inputerror'][] = 'txt_enquiry_subject';
            $data['error_string'][] = 'Please enter enquiry subject';
            $data['status'] = FALSE;
        }

        if($this->input->post('txt_enquiry_details') == '')
        {
            $data['inputerror'][] = 'txt_enquiry_details';
            $data['error_string'][] = 'Please enter enquiry details';
            $data['status'] = FALSE;
        }
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	public function update_status()
	{
		//change the status i.e., isActive or Active
    	$status = $this->input->post('status');
		$c_id = $this->input->post('id');
		$this->Enquiries_model->update_status($c_id,$status);
	}
	public function view_details($enquiry_id)
	{
	
		$data['enquiry_details']=$this->Enquiries_model->get_enquiry_details($enquiry_id);
		$this->load->view('admin/Enquiry_details_view',$data);
	}
}
?>