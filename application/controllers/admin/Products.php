<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Products_model');
		          $this->load->helper(array('form', 'url'));
	   }
	   
	public function index()
	{
			$data['role'] = $this->Products_model->show_roles();
			$this->load->view('admin/Products_view',$data);		
	}
	
	public function ajax_list()
    {
  		$list = $this->Products_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $Product) {
		$view = base_url()."admin/products/view_details/".$Product->product_id;
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$Product->product_id.'">';
			$row[] = $no;
            $row[] = '<a href='."'".$view."'".' data-fancybox data-type="iframe" title="View Details" style="text-align: center;">'.$Product->product_name.'</a>';
			$row[] = substr($Product->product_description,0,200).' ...';
			if($Product->product_image)
                $row[] = '<a href="'.base_url('upload/product/'.$Product->product_image).'" target="_blank"><img src="'.base_url('upload/product/'.$Product->product_image).'" class="img-responsive" width="50" height="50" /></a>';
            else
                $row[] = '(No photo)';
			
			$status_class = ($Product->status?"btn-success":"btn-danger"); // returns true
			$status =($Product->status? "Active" : "Inactive");
			$row[] = '<i data='."'".$Product->product_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$Product->product_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$Product->product_id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Products_model->count_all(),
                        "recordsFiltered" => $this->Products_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	
	public function ajax_edit($id)
    {
        $data = $this->Products_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
		 $data = array(
                'product_name' => $this->input->post('productName'),
                'product_description' => $this->input->post('productDescription'),
				//'product_price' => $this->input->post('productPrice'),
				//'product_tax' => $this->input->post('productTax'),
				//'product_quantity' => $this->input->post('productQuantity'),
				'added_date' => date("Y-m-d H:i:s"),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		
		if(!empty($_FILES['product_image']['name']))
        {
            $upload = $this->_do_upload();
            $data['product_image'] = $upload;
        }
		$insert = $this->Products_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		$data = array(
                'product_name' => $this->input->post('productName'),
                'product_description' => $this->input->post('productDescription'),
				//'product_price' => $this->input->post('productPrice'),
				//'product_tax' => $this->input->post('productTax'),
				//'product_quantity' => $this->input->post('productQuantity'),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
			
		if($this->input->post('remove_photo')) // if remove photo checked
        {
            if(file_exists('upload/product/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
                unlink('upload/product/'.$this->input->post('remove_photo'));
            $data['product_image'] = '';
        }
 
        if(!empty($_FILES['product_image']['name']))
        {
            $upload = $this->_do_upload();
             
            //delete file
            $person = $this->Products_model->get_by_id($this->input->post('id'));
            if(file_exists('upload/product/'.$person->product_image) && $person->product_image)
                unlink('upload/product/'.$person->product_image);
 
            $data['product_image'] = $upload;
        }
		$this->Products_model->update(array('product_id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->Products_model->get_by_id($id);
        if(file_exists('upload/product/'.$person->product_image) && $person->product_image)
        unlink('upload/product/'.$person->product_image);
		$this->Products_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Products_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
	 private function _do_upload()
    {
        $config['upload_path']          = 'upload/product/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['create_thumb'] 		= TRUE;
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 		
        //$this->load->library('upload', $config);
 		$this->upload->initialize($config);
        if(!$this->upload->do_upload('product_image')) //upload and validate
        {
            $data['inputerror'][] = 'product_image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }
	 
	
	 private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('productName') == '')
        {
            $data['inputerror'][] = 'productName';
            $data['error_string'][] = 'Product name is required';
            $data['status'] = FALSE;
        }
		/*if($this->input->post('productPrice') == '')
        {
            $data['inputerror'][] = 'productPrice';
            $data['error_string'][] = 'product price is required';
            $data['status'] = FALSE;
        }
		if($this->input->post('productTax') == '')
        {
            $data['inputerror'][] = 'productTax';
            $data['error_string'][] = 'product tax is required';
            $data['status'] = FALSE;
        }
		if($this->input->post('productQuantity') == '')
        {
            $data['inputerror'][] = 'productQuantity';
            $data['error_string'][] = 'product quantity is required';
            $data['status'] = FALSE;
        }*/
		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	 public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('product_id');
		        $this->Products_model->update_status($c_id,$status);
	}
	public function view_details($product_id)
	{
	
		$data['product_details']=$this->Products_model->get_product_details($product_id);
		$this->load->view('admin/Product_details_view',$data);
	}
}
?>