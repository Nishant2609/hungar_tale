<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends CI_Controller 
{

	function __construct() 
	   {
		          parent::__construct();
		          $this->load->model('admin/Clients_model');
		          $this->load->helper(array('form', 'url'));
	   }
	   
	public function index()
	{
			$data['role'] = $this->Clients_model->show_roles();
			$this->load->view('admin/Clients_view',$data);		
	}
	
	public function ajax_list()
    {
  		$list = $this->Clients_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $Client) {
            $no++;
            $row = array();
			$row[] ='<input type="checkbox" class="data-check" value="'.$Client->client_id.'">';
			$row[] = $no;
            $row[] = $Client->client_name;
			$row[] = $Client->client_email;
			$row[] = $Client->client_mobile;
			$row[] = $Client->client_address;
			if($Client->client_image)
                $row[] = '<a href="'.base_url('upload/client/'.$Client->client_image).'" target="_blank"><img src="'.base_url('upload/client/'.$Client->client_image).'" class="img-responsive" width="50" height="50" /></a>';
            else
                $row[] = '(No photo)';
			
			$status_class = ($Client->status?"btn-success":"btn-danger"); // returns true
			$status =($Client->status? "Active" : "Inactive");
			$row[] = '<i data='."'".$Client->client_id."'".' class="status_checks btn btn-sm '.$status_class.'"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></i>';
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$Client->client_id."'".')"><i class="fa fa-pencil-square-o"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user('."'".$Client->client_id."'".')"><i class="glyphicon glyphicon-trash"></i> </a>';
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Clients_model->count_all(),
                        "recordsFiltered" => $this->Clients_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function content_search()
	{
			$this->load->view('admin/Content_search_view');
	}
	
	public function ajax_edit($id)
    {
        $data = $this->Clients_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
         $this->_validate();
		 $data = array(
                'client_name' => $this->input->post('clientName'),
                'client_email' => $this->input->post('clientEmail'),
				'client_address' => $this->input->post('clientAddress'),
				'client_mobile' => $this->input->post('clientMobile'),
				'added_date' => date("Y-m-d H:i:s"),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
		
		if(!empty($_FILES['client_image']['name']))
        {
            $upload = $this->_do_upload();
            $data['client_image'] = $upload;
        }
		$insert = $this->Clients_model->save($data);
        echo json_encode(array("status" => TRUE, "message"=>"Record Insert Successfully"));
    }
 
    public function ajax_update()
    {
        $this->_validate();
		$data = array(
                'client_name' => $this->input->post('clientName'),
                'client_email' => $this->input->post('clientEmail'),
				'client_address' => $this->input->post('clientAddress'),
				'client_mobile' => $this->input->post('clientMobile'),
				'modified_date' => date("Y-m-d H:i:s"),
				'status'=>'1'
            );
			
		if($this->input->post('remove_photo')) // if remove photo checked
        {
            if(file_exists('upload/client/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
                unlink('upload/client/'.$this->input->post('remove_photo'));
            $data['client_image'] = '';
        }
 
        if(!empty($_FILES['client_image']['name']))
        {
            $upload = $this->_do_upload();
             
            //delete file
            $person = $this->Clients_model->get_by_id($this->input->post('id'));
            if(file_exists('upload/client/'.$person->client_image) && $person->client_image)
                unlink('upload/client/'.$person->client_image);
 
            $data['client_image'] = $upload;
        }
		$this->Clients_model->update(array('client_id' => $this->input->post('id')), $data);
         echo json_encode(array("status" => TRUE, "message"=>"Record Update Successfully"));
    }
 
    public function ajax_delete($id)
    {
        //delete file
        $person = $this->Clients_model->get_by_id($id);
        if(file_exists('upload/client/'.$person->client_image) && $person->client_image)
        unlink('upload/client/'.$person->client_image);
		$this->Clients_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_bulk_delete()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->Clients_model->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }
	
	 private function _do_upload()
    {
        $config['upload_path']          = 'upload/client/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['create_thumb'] 		= TRUE;
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 		
        //$this->load->library('upload', $config);
 		$this->upload->initialize($config);
        if(!$this->upload->do_upload('client_image')) //upload and validate
        {
            $data['inputerror'][] = 'client_image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }
	 
	
	 private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('clientName') == '')
        {
            $data['inputerror'][] = 'clientName';
            $data['error_string'][] = 'Customer name is required';
            $data['status'] = FALSE;
        }
		if($this->input->post('clientEmail') == '')
        {
            $data['inputerror'][] = 'clientEmail';
            $data['error_string'][] = 'customer email is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('clientMobile') == '')
        {
            $data['inputerror'][] = 'clientMobile';
            $data['error_string'][] = 'Mobile is required';
            $data['status'] = FALSE;
        }
        else if(strlen($this->input->post('clientMobile')) != 10)
        {
            $data['inputerror'][] = 'clientMobile';
            $data['error_string'][] = '10 digit are required';
            $data['status'] = FALSE;
        }
		if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	 public function update_status()
	{
				//change the status of the firm i.e., isActive or Active
    			$status = $this->input->post('status');
				$c_id = $this->input->post('client_id');
		        $this->Clients_model->update_status($c_id,$status);
	}

}
?>