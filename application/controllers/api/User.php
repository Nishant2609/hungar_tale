<?php

require APPPATH.'libraries/REST_Controller.php';

class User extends REST_Controller{

  public function __construct(){

    parent::__construct();
    //load database
    $this->load->database();
    $this->load->model(array("api/User_model"));
    $this->load->library(array("form_validation"));
    $this->load->helper("security");
    $this->load->helper('text');
  }

  /*
    INSERT: POST REQUEST TYPE
    UPDATE: PUT REQUEST TYPE
    DELETE: DELETE REQUEST TYPE
    LIST: Get REQUEST TYPE
  */

  // POST: <project_url>/index.php/student
  public function index_post(){
    // insert data method

    //print_r($this->input->post());die;

    // collecting form data inputs
    $first_name = $this->security->xss_clean($this->input->post("first_name"));
    $last_name = $this->security->xss_clean($this->input->post("last_name"));
    $email = $this->security->xss_clean($this->input->post("email"));
    $mobile = $this->security->xss_clean($this->input->post("mobile"));
   $country_code = $this->security->xss_clean($this->input->post("country_code"));

    // form validation for inputs
    $this->form_validation->set_rules("first_name", "First Name", "required");
    $this->form_validation->set_rules("last_name", "Last Name", "required");
    $this->form_validation->set_rules("email", "Email", "required|valid_email");
    $this->form_validation->set_rules("mobile", "Mobile", "required");
    $this->form_validation->set_rules("country_code", "Country code", "required");

    // checking form submittion have any error or not
    if($this->form_validation->run() === FALSE){

      // we have some errors
      $this->response(array(
        "status" => 0,
        "message" => "All fields are needed"
      ) , REST_Controller::HTTP_NOT_FOUND);
    }else{

      if(!empty($first_name) && !empty($last_name) && !empty($email) && !empty($mobile) && !empty($country_code)){
        // all values are available
        $student = array(
          "first_name" => $first_name,
          "last_name" => $last_name,
          "email" => $email,
          "mobile" => $mobile,
          "country_code" => $country_code
        );

        if($this->User_model->insert_student($student)){

          $this->response(array(
            "status" => 1,
            "message" => "Student has been created"
          ), REST_Controller::HTTP_OK);
        }else{

          $this->response(array(
            "status" => 0,
            "message" => "Failed to create student"
          ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
      }else{
        // we have some empty field
        $this->response(array(
          "status" => 0,
          "message" => "All fields are needed"
        ), REST_Controller::HTTP_NOT_FOUND);
      }
    }

    /*$data = json_decode(file_get_contents("php://input"));

    $name = isset($data->name) ? $data->name : "";
    $email = isset($data->email) ? $data->email : "";
    $mobile = isset($data->mobile) ? $data->mobile : "";
    $course = isset($data->course) ? $data->course : "";*/


  }

  // PUT: <project_url>/index.php/student
  public function index_put(){
    // updating data method
    //echo "This is PUT Method";
    $data = json_decode(file_get_contents("php://input"));

    if(isset($data->id) && isset($data->name) && isset($data->email) && isset($data->mobile) && isset($data->course)){

      $student_id = $data->id;
      $student_info = array(
        "name" => $data->name,
        "email" => $data->email,
        "mobile" => $data->mobile,
        "course" => $data->course
      );

      if($this->User_model->update_student_information($student_id, $student_info)){

          $this->response(array(
            "status" => 1,
            "message" => "Student data updated successfully"
          ), REST_Controller::HTTP_OK);
      }else{

        $this->response(array(
          "status" => 0,
          "messsage" => "Failed to update student data"
        ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
      }
    }else{

      $this->response(array(
        "status" => 0,
        "message" => "All fields are needed"
      ), REST_Controller::HTTP_NOT_FOUND);
    }
  }

  // DELETE: <project_url>/index.php/student
  public function index_delete(){
    // delete data method
    $data = json_decode(file_get_contents("php://input"));
    $student_id = $this->security->xss_clean($data->student_id);

    if($this->User_model->delete_student($student_id)){
      // retruns true
      $this->response(array(
        "status" => 1,
        "message" => "Student has been deleted"
      ), REST_Controller::HTTP_OK);
    }else{
      // return false
      $this->response(array(
        "status" => 0,
        "message" => "Failed to delete student"
      ), REST_Controller::HTTP_NOT_FOUND);
    }
  }
// User's Data 
// GET: <project_url>/index.php/student
  public function getCountryCode_post(){
    // list data method
    //echo "This is GET Method";
    // SELECT * from tbl_students;
    $jsonStr=file_get_contents("php://input");
    $this->db->query('insert into log(post_data,created) values('.$this->db->escape($jsonStr).',now())');
    $data = json_decode($jsonStr);
    //print_r($data);die;
    //$this->db->query('insert into log(post_data,created) values('.$this->db->escape($data).',now())');
    $country_name=$data->country;
    $country_code = $this->User_model->get_country_code($country_name);
    

    //print_r($query->result());

    if(count($country_code) > 0){

      $this->response(array(
        "status" => 1,
        "message" => "Country code found",
        "data" => $country_code
      ), REST_Controller::HTTP_OK);
    }else{

      $this->response(array(
        "status" => 0,
        "message" => "No Country code found",
        "data" => $country_code
      ), REST_Controller::HTTP_NOT_FOUND);
    }
  }
  // GET: <project_url>/index.php/student
  public function getUserMobile_post(){
    // list data method
    //echo "This is GET Method";
    // SELECT * from tbl_students;
    //$data = json_decode(file_get_contents("php://input"));
    //print_r($data);die;
    $jsonStr=file_get_contents("php://input");
    $this->db->query('insert into log(post_data,created) values('.$this->db->escape($jsonStr).',now())');
    $data = json_decode($jsonStr);
    $mobile_no=$data->mobile;
    $user_mobile = $this->User_model->get_mobile_no($mobile_no);
    //print_r($query->result());
    if(count($user_mobile) > 0){

      $this->response(array(
        "status" => 1,
        "message" => "User found",
        "data" => $user_mobile
      ), REST_Controller::HTTP_OK);
    }else{

      $this->response(array(
        "status" => 0,
        "message" => "No user found",
        "data" => $user_mobile
      ), REST_Controller::HTTP_NOT_FOUND);
    }

  }

  // PUT: <project_url>/index.php/student
  public function addUser_post(){
    // updating data method
    //echo "This is PUT Method";
   // $data = json_decode(file_get_contents("php://input"));
   $jsonStr=file_get_contents("php://input");
   $this->db->query('insert into log(post_data,created) values('.$this->db->escape($jsonStr).',now())');
   $data = json_decode($jsonStr);
    if(!empty($data->first_name) && !empty($data->last_name) && !empty($data->email) && !empty($data->mobile) && !empty($data->country_code)){

      //$student_id = $data->id;
     $user_info = array(
          "first_name" => $data->first_name,
          "last_name" => $data->last_name,
          "email" => $data->email,
          "mobile" => $data->mobile,
          "country_code" => $data->country_code
        );
        
      if($this->User_model->insert_user($user_info)){
        $user_details=$this->User_model->get_user_details($data->mobile);
       // echo $this->db->last_query();
          $this->response(array(
            "status" => 1,
            "message" => "User data added successfully",
            "data" => $user_details
          ), REST_Controller::HTTP_OK);
      }else{

        $this->response(array(
          "status" => 0,
          "messsage" => "Failed to update student data",
          "data" => $user_details
        ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
      }
    }else{

      $this->response(array(
        "status" => 0,
        "message" => "All fields are needed",
        "data" => $user_details
      ), REST_Controller::HTTP_NOT_FOUND);
    }
  }


    // GET: <project_url>/index.php/student
    public function getUserInfo_post(){
      // list data method
      //echo "This is GET Method";
      // SELECT * from tbl_students;
      //$data = json_decode(file_get_contents("php://input"));
      //print_r($data);die;
      $jsonStr=file_get_contents("php://input");
      $this->db->query('insert into log(post_data,created) values('.$this->db->escape($jsonStr).',now())');
      $data = json_decode($jsonStr);
      $mobile_no=$data->mobile;
      $user_mobile = $this->User_model->get_user_details($mobile_no);
      if(!empty($user_mobile)){
  
        $this->response(array(
          "status" => 1,
          "message" => "User details found",
          "data" => $user_mobile
        ), REST_Controller::HTTP_OK);
      }else{
  
        $this->response(array(
          "status" => 0,
          "message" => "No user details found",
          "data" => $user_mobile
        ), REST_Controller::HTTP_NOT_FOUND);
      }
  
    }

    //get slider images
  public function getWelcomeImageUrls_post(){    
    $slider_img = $this->User_model->get_slider_images();
    
    if(count($slider_img) > 0){

      $this->response(array(
        "status" => 1,
        "message" => "Slider images are found",
        "data" => $slider_img
      ), REST_Controller::HTTP_OK);
    }else{

      $this->response(array(
        "status" => 0,
        "message" => "No slider images are found",
        "data" => $slider_img
      ), REST_Controller::HTTP_NOT_FOUND);
    }

  }

  //get list of popular and all cuisines
  public function getPopularCuisines_post(){    
    $cuisines["popular_cuisines"] = $this->User_model->get_popular_cuisines();
    $cuisines["all_cuisines"] = $this->User_model->get_all_cuisines();
    //get_all_cuisines
    
    if(count($cuisines) > 0){

      $this->response(array(
        "status" => 1,
        "message" => "Popular cuisines are found",
        "data" => $cuisines
      ), REST_Controller::HTTP_OK);
    }else{

      $this->response(array(
        "status" => 0,
        "message" => "No Popular cuisines are found",
        "data" => $cuisines
      ), REST_Controller::HTTP_NOT_FOUND);
    }

  }

  //get slider images
  public function getHomeBannerImage_post(){    
    $slider_img = $this->User_model->get_slider_home();
    
    if(count($slider_img) > 0){

      $this->response(array(
        "status" => 1,
        "message" => "Banner images are found",
        "data" => $slider_img
      ), REST_Controller::HTTP_OK);
    }else{

      $this->response(array(
        "status" => 0,
        "message" => "No Banner images are found",
        "data" => $slider_img
      ), REST_Controller::HTTP_NOT_FOUND);
    }



  }
   
    // Update user profile
  public function updateProfile_post(){
  // updating data method
  
  //$data = json_decode(file_get_contents("php://input"));
   $jsonStr=file_get_contents("php://input");
    //  $this->db->query('insert into log(post_data,created) values('.$this->db->escape($jsonStr).',now())');
      $data = json_decode($jsonStr);

  if(!empty($data->first_name) && !empty($data->last_name) && !empty($data->email) && !empty($data->profile_image)){
    // !empty($data->mobile) &&
    $user_id = $data->user_id;
    $image=base64_decode($data->profile_image);
   //$image = base64_decode($this->input->post("img_front"));
$image_name = round(microtime(true) * 1000);
$filename = $image_name . '.jpeg';
//rename file name with random number
$path = "uploads/user_profile_images/".$filename;
//image uploading folder path
file_put_contents($path, $image);
$base  = "http://".$_SERVER['HTTP_HOST'];
$base .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
$base .=$path;
   $user_info = array(
    "user_id" => $data->user_id,
        "first_name" => $data->first_name,
        "last_name" => $data->last_name,
        "email" => $data->email,
       /*  "mobile" => $data->mobile, */
        "profile_image" => $base
      );

    if($this->User_model->update_user_information($user_id, $user_info)){

        $this->response(array(
          "status" => 1,
          "message" => "user data updated successfully",
           "data" => $user_info
        ), REST_Controller::HTTP_OK);
    }else{

      $this->response(array(
        "status" => 0,
        "messsage" => "Failed to update user data"
      ), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
    }
  }else{

    $this->response(array(
      "status" => 0,
      "message" => "All fields are needed"
    ), REST_Controller::HTTP_NOT_FOUND);
  }
}
 

  
  // where its end
}

 ?>
