<?php $this->load->view('admin/commonfiles/header'); ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h2 class="panel-title" align="center"><b>Product details</b></h2>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table  class="table table-striped" cellpadding="0">
        <?php
			if(isset($product_details))
			{
			foreach($product_details as $product_det)
			{ 
			?>
        <tr>
          <th>Name</th>
          <td><?php echo $product_det->product_name;?></td>
        </tr>
        <?php
                         if($product_det->product_image!="")
                         {
						 ?>
        <tr>
          <th>Image</th>
          <td><a href="<?php echo base_url('upload/product/'.$product_det->product_image);?>" target="_blank"><img src="<?php echo base_url('upload/product/'.$product_det->product_image);?>" width="70px" height="50px" /></a></td>
        </tr>
        <?php
                         }
						 ?>
        <tr>
          <th>Description</th>
          <td><?php echo $product_det->product_description;?></td>
        </tr>
        <?php
			}
			}
			?>
      </table>
    </div>
  </div>
</div>
