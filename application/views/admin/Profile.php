<?php $this->load->view('admin/commonfiles/header'); ?>
<?php $this->load->view('admin/commonfiles/menu'); ?>
<script>
$(document).ready (function(){
            $("#success-alert1").hide();
            $("#btn_submit").click(function showAlert() {
                $("#success-alert1").alert();
                $("#success-alert1").fadeTo(5000, 500).slideUp(500, function(){
               $("#success-alert1").slideUp(500);
                });   
            });
 });
</script>
<style>
.popupunder{
    width: 300px;
	position:fixed;
	top: 60px;
	right: 10px;
	z-index: 10;
	border: 0;
	padding: 20px;
}
.popupunder.alert-success{
    border: 1px solid #198b49;
	background:#27AE60;
	color:#fff;
}
.popupunder .close{
	font-size: 10px;
	position:absolute !important;
	right: 2px;
	top: 3px;
}
.p-view{  font-size: 15px;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: left;}
</style>
	 <?php 
   if($this->session->flashdata('editmsg')) {
   ?>
<div class="container">
<div class="row">
		<div class="popupunder alert alert-success fade in" id="success-alert1"><button type="button" class="close close-sm" data-dismiss="alert"><i class="glyphicon glyphicon-remove"></i></button><strong>Success : </strong> 
<?php
echo ''.$this->session->flashdata('editmsg').'';?> 
</div>
	</div>
</div>
<?php } ?>
		<!-- Main Container starts here -->
        
        <div class="row">
 <div class="col-lg-12" align="center">
  <font size="+3" color="#0066CC">My profile</font>
 

 </div>
 </div>
            <br/>
		<div id="wrapper">
    <div id="page-content-wrapper">
      <div class="container-fluid">
      
            <div class="panel panel-info">
             <!-- <div class="panel-heading">
                <h2 class="panel-title" align="center"><b>Personal details</b></h2>
              </div>-->
              <div class="panel-body" align="center">
                <?php 
				foreach($records as $record)
				{
			?>
	<div class="row">
	      	<!-- profile pic container starts here -->
				<div class="col-sm-12 col-md-5">
					<div class="widget-box">
						
						<div class="widget-container text-center">
                        
							  <?php
                        if($record->image=="")
								{
						?>
									 <img src="<?php echo base_url();?>assets/images/user_image.png"  alt="User Image"  width="240px" height="220px" class="img-circle"/>
					  <?php
								}
								else
								{
					  ?>
							<img src="<?php echo base_url('upload/'.  $record->image);?>"  alt="User Image"  width="240px" height="220px" class="img-circle"/>
								
					  <?php
								}
								?>
                             <br/><br/>
							<div class="btn-group">
                            <a class="btn btn-info"  href="<?php echo base_url()."index.php/admin/Login/profile_edit_view"?>">Edit profile</a>
							  
							</div>
						</div>
					</div>
				</div>
				<!-- profile pic container End here -->
				
				<!-- personal info container starts here -->
				<div class="col-sm-12 col-md-6" >
					<div class="widget-box">
						
                        
                        <form class="form-horizontal">
                <div  style="border-bottom:1px solid #CCC;">
              <label class="col-md-3 control-label" for="name" style="text-align:right;">First name : </label>
              <div class="col-md-8" style="padding-left:0px;">
               <p class="p-view"> <?php echo $record->first_name; ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            	<div  style="border-bottom:1px solid #CCC;">
              <label class="col-md-3 control-label" for="name" style="text-align:right;">Last name : </label>
              <div class="col-md-8" style="padding-left:0px;">
               <p class="p-view"> <?php echo $record->last_name; ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            
            
            
                <div  style="border-bottom:1px solid #CCC;">
              <label class="col-md-3 control-label" for="name" style="text-align:right;">Email :</label>
              <div class="col-md-8" style="padding-left:0px;">
               <p class="p-view"> <?php echo $record->email; ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            
            <div  style="border-bottom:1px solid #CCC;">
              <label class="col-md-3 control-label" for="name" style="text-align:right;">Address :</label>
              <div class="col-md-8" style="padding-left:0px;">
               <p class="p-view"> <?php echo $record->address; ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            
           
            
             <div  style="border-bottom:1px solid #CCC;">
              <label class="col-md-3 control-label" for="name" style="text-align:right;">Mobile :</label>
              <div class="col-md-8" style="padding-left:0px;">
               <p class="p-view"> <?php echo $record->mobile; ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            
             <div  style="border-bottom:1px solid #CCC;">
              <label class="col-md-3 control-label" for="name" style="text-align:right;">Status :</label>
              <div class="col-md-8" style="padding-left:0px;">
               <p class="p-view">
                <?php
			     if($record->status=='1')
				{
				echo "Active";
				}
				else
				{
				echo "In-Active";
				}
			   ; ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            
                <div class="clearfix"></div>
                </form>
                
                
					</div>
				</div>
				<!-- personal info container End here -->
                 <?php
					}
					?>
			</div>
            </div>
            </div>
            </div>
            </div>
		<?php $this->load->view('admin/commonfiles/footer'); ?>	
			
		