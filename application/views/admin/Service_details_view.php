<?php $this->load->view('admin/commonfiles/header'); ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h2 class="panel-title" align="center"><b>Service details</b></h2>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table  class="table table-striped" cellpadding="0">
        <?php
			if(isset($service_details))
			{
			foreach($service_details as $service_det)
			{ 
			?>
        <tr>
          <th>Name</th>
          <td><?php echo $service_det->service_name;?></td>
        </tr>
        <?php
                         if($service_det->service_image!="")
                         {
						 ?>
        <tr>
          <th>Image</th>
          <td><a href="<?php echo base_url('upload/service/'.$service_det->service_image);?>" target="_blank"><img src="<?php echo base_url('upload/service/'.$service_det->service_image);?>" width="70px" height="50px" /></a></td>
        </tr>
        <?php
                         }
						 ?>
        <tr>
          <th>Description</th>
          <td><?php echo $service_det->service_description;?></td>
        </tr>
        <?php
			}
			}
			?>
      </table>
    </div>
  </div>
</div>
