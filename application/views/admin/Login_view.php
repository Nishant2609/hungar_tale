<?php $this->load->view('admin/commonfiles/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/Login.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery_validations/admin/login_validation.js"></script>

<style>
label.error {
  width: 100%;
  color: red;
  font-style: italic;
  text-align:left;
  float:left;
}
</style>
<!--<script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>
--><body>
 <div class="container">
                <div class="row vertical-offset-100">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">                                
                               <div class="header_txt"><h3>Hunger Tale Admin Login</h3></div>
                            </div>
                            <div class="panel-body">
                                <form accept-charset="UTF-8" role="form" class="form-signin" name="loginform" id="loginform" method="post" action="<?php echo base_url();?>admin/Login/login">
                                    <fieldset>
                                        <label class="panel-login">
                                            <div class="login_result">
                                            <?php if(isset($error_message))
											{
											echo "<h4>".$error_message."</h4>";
											}
											?>
                                            </div>
                                        </label>
                                        <input class="form-control" placeholder="Please enter email" id="email" type="text" name="email">
                                        <input class="form-control" placeholder="Please enter password" id="password" type="password" name="password">
                                        <br>
                                        <input class="btn btn-lg btn-success btn-block" type="submit" id="login" value="Login">
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</body>