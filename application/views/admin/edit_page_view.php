<?php $this->load->view('admin/commonfiles/header'); ?>
<link rel="stylesheet" href="<?php echo base_url();?>css/breadcrumbs.css">
<?php $this->load->view('admin/commonfiles/menu'); ?>
<!--<script src="<?php echo base_url(); ?>/ckeditor/ckeditor.js"></script>-->
<script>
$(document).ready (function(){
            $("#success-alert1").hide();
            $("#btn_submit").click(function showAlert() {
                $("#success-alert1").alert();
                $("#success-alert1").fadeTo(5000, 3000).slideUp(3000, function(){
               $("#success-alert1").slideUp(3000);
                });   
            });
 });
</script>
<style>
.popupunder {
	width: 300px;
	position:fixed;
	top: 60px;
	right: 10px;
	z-index: 10;
	border: 0;
	padding: 20px;
}
.popupunder.alert-success {
	border: 1px solid #198b49;
	background:#27AE60;
	color:#fff;
}
.popupunder .close {
	font-size: 10px;
	position:absolute !important;
	right: 2px;
	top: 3px;
}
.p-view {
	font-size: 15px;
	margin-bottom: 0;
	padding: 6px 12px;
	text-align: left;
}
</style>
<style>
input.error {
	border: 1px dotted red;
}
label.error {
	width: 100%;
	color: red;
	font-style: italic;
	text-align:left;
	float:left;
	margin-bottom: 5px;
}
</style>
<style>
.btn-glyphicon {
	padding:8px;
	background:#ffffff;
	margin-right:4px;
}
.icon-btn {
	padding: 1px 15px 3px 2px;
	border-radius:50px;
}
</style>
<style>
.fstQueryInputExpanded {
	width:100%;
}
.fstElement {
	font-size: 0.7em;
	width:100%;
}
.fstToggleBtn {
	min-width: 16.5em;
}
.submitBtn {
	display: none;
}
.fstMultipleMode {
	display: block;
}
.fstMultipleMode .fstControls {
	width: 100%;
}
ul, li {
	margin:0;
	padding:0;
	list-style:none;
}
</style>
<style>
#wait {
	display: none;
}
</style>
<?php 
   if($this->session->flashdata('editmsg')) {
   ?>
<div class="container">
  <div class="row">
    <div class="popupunder alert alert-success fade in" id="success-alert1">
      <button type="button" class="close close-sm" data-dismiss="alert"><i class="glyphicon glyphicon-remove"></i></button>
      <strong>Success : </strong>
      <?php
echo ''.$this->session->flashdata('editmsg').'';?>
    </div>
  </div>
</div>
<?php } ?>
<div id="wrapper">
  <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12" align="center"> <font size="+2" color="#0066CC" class="blinking">Edit page</font> </div>
      </div>
      <div class="col-lg-9" align="center"> </div>
      <div align="right"> <a class="btn btn-success"  href="<?php echo base_url()."admin/page_details"?>"><span class="glyphicon btn-glyphicon glyphicon-arrow-left img-circle text-info"></span>Back</a> </div>
      <br/>
      <!--bread crumbs Start Here..-->
      <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="<?php echo base_url()."admin/welcome"?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>
        <li><a href="<?php echo base_url()."admin/page_details"?>">Pages</a></li>
        <li class="active"><span>Edit page</span></li>
      </ol>
      <!--end of bread crumbs-->
      <!--  Add task form is start-->
      <div class="row">
        <div class="col-md-12">
        <form method="post" class="form-horizontal" enctype="multipart/form-data" id="edit_page" name="edit_page" action="<?php echo base_url()."admin/page_details/edit_page";?>">
          <?php
                  foreach($records as $page_detail)
                  {
		?>
          <input type="hidden" name="tg_txt_pcid" id="tg_txt_pcid" value="<?php echo $page_detail->page_content_id; ?>" />
          <div class="panel panel-info">
            <div class="panel-body">
              <div id="taskmsg" style="color:#FF0000"> </div>
              <div id="taskaddmsg" style="color:#FF0000"> </div>
              <div class="step1">
                <div class="form-group">
                  <label class="control-label col-md-3">Page title : <span class="required" aria-required="true" style="color:red;">*</span></label>
                  <div class="col-md-6">
                    <input type="text" class="form-control"  placeholder=" Enter page title" name="tg_txt_page_title" id="tg_txt_page_title" value="<?php echo $page_detail->page_title;?>" readonly="readonly">
                    <span class="help-block"></span> </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Page content:</label>
                  <div class="col-md-6">
                    <textarea class="form-control"  placeholder=" Enter Page Content" name="tg_txt_page_content" id="tg_txt_page_content"><?php echo $page_detail->page_content;?></textarea>
                    <span class="help-block"></span> </div>
                </div>
                <script>
				CKEDITOR.replace( 'tg_txt_page_content' );
			    </script>
                <div class="form-group">
                  <label class="control-label col-md-3">Status:</label>
                  <div class="col-md-6">
                    <?php
									if($page_detail->is_active==1)
									{
									?>
                    <input type="checkbox" name="tg_chk_is_active" id="tg_chk_is_active" value="1" checked="checked"/>
                    <?php
				}
				else
				{
				?>
                    <input type="checkbox" name="tg_chk_is_active" id="tg_chk_is_active" value="1"/>
                    <?php
				}
				?>
                    <span style="margin-left:10px;">
                    <label>Is active</label>
                    </span> </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3"></label>
                <div class="col-md-6">
                  <input type="Submit" class="btn btn-success" name="btn_submit" id="btn_submit" value="Submit"/>
                </div>
              </div>
              <?php }?>
            </div>
          </div>
          </div>
        </form>
      </div>
    </div>
    <!--row -->
    <!--  Add task form closed-->
  </div>
</div>
</div>
<?php $this->load->view('admin/commonfiles/footer'); ?>
