<?php $this->load->view('admin/commonfiles/header'); ?>
<?php $this->load->view('admin/commonfiles/menu'); ?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/breadcrumbs.css">
<style>
.user-search-input
{
width:100%;
}
</style>
<style>
label.error {
	width: 100%;
	color: red;
	font-style: italic;
	text-align:left;
	margin-bottom: 5px;
}
</style>

<script type="text/javascript">
$(document).on('click','.status_checks',function(){
      var status=($(this).hasClass("btn-success")) ? '0' : '1';
	   var msg=(status=='0')? 'Deactivate' : 'Activate';
	  
	  if(confirm("Are you sure to "+ msg)){
        var current_element=$(this);
		var id=$(current_element).attr('data');
	
		
       var myurl="<?php echo base_url()."admin/User_detail/update_status"?>";
        $.ajax({
          type:"POST",
          url:myurl,
          data:{"user_id":id,"status":status},
          success:function(data)
          {   
		  
            location.reload();
          }
        });
      }      
    });
</script>	

    <div class="row">
          <div class="col-lg-12" align="center">
          		<font size="+2" color="#0066CC" class="blinking">User details</font>
          </div>
    </div>
              <ol class="breadcrumb breadcrumb-arrow">
                <li><a href="<?php echo base_url()."admin/welcome"?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                <li class="active"><span>User details</span></li>
              </ol>
  
     		  <button class="btn btn-success" onclick="add_user()"><i class="glyphicon glyphicon-plus"></i> Add User</button>
             <button class="btn btn-danger" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk delete</button>
      
        <br/> 
        <br/> 
        
 <div class="panel panel-info">             
      <div class="panel-body"> 
      
        <div class="table-responsive">
         <table id="table" class="table table-striped table-bordered" cellpadding="0"  width="100%">
                <thead>
                    <tr>
                    <th>Select all<br/><input type="checkbox" id="check-all"></th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone number</th>
                    <th>Role</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
	        	</thead>
                 <thead>
					<tr>
						<td>-</td>
						<td><input type="text" id="1" class="user-search-input"></td>
						<td><input type="text" id="2" class="user-search-input" ></td>
						<td><input type="text" id="3" class="user-search-input" ></td>
						<td><input type="text" id="4" class="user-search-input" ></td>
                        <td><input type="text" id="5" class="user-search-input" ></td>
						<td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
					</tr>
				</thead>
               
	        <tbody>
	        </tbody>
	      </table>
		</div>
    </div>
  </div>	


<script type="text/javascript">

    var save_method; //for save method string
    var table;
	var base_url = '<?php echo base_url();?>';
    $(document).ready(function() {
      table = $('#table').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('admin/User_detail/ajax_list')?>",
            "type": "POST"
        },
		aLengthMenu: [
        [100,125, 150, 200, -1],
        [100,125, 150, 200, "All"]
    ],
        //Set column definition initialisation properties.
        "columnDefs": [
        {
          "targets": [ 0,-1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

		dom: 'lBfrtip',
		"buttons": [
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ]
		
      });
	  
	  			$("#table_filter").css("display","none");  // hiding global search box
				
				$('.user-search-input').on( 'keyup click change', function () {   
					var i =$(this).attr('id');  // getting column index
					var v =$(this).val();  // getting search input value
					table.columns(i).search(v).draw();
				} );
	   //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
	
	//check all
    $("#check-all").click(function () {
        $(".data-check").prop('checked', $(this).prop('checked'));
    });
 
});


    function add_user()
	{
		save_method = 'add';
		$('#form')[0].reset(); // reset form on modals
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#modal_form').modal('show'); // show bootstrap modal
		$('.modal-title').text('Add user'); // Set Title to Bootstrap modal title
		$('#photo-preview').hide(); // hide photo preview modal
 		$('#label-photo').text('Upload photo'); // label photo upload
	}
 
function edit_user(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('admin/User_detail/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 			
            $('[name="id"]').val(data.user_id);
            $('[name="firstName"]').val(data.first_name);
            $('[name="lastName"]').val(data.last_name);
			 $('[name="email"]').val(data.email);
            $('[name="password"]').val(data.password);
			$('[name="address"]').val(data.address);
            $('[name="mobile"]').val(data.mobile);
			$('[name="role"]').val(data.role_id);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title
 			$('#photo-preview').show(); // show photo preview modal
 			
            if(data.image)
            {
                $('#label-photo').text('Change photo'); // label photo upload
                $('#photo-preview div').html('<img src="'+base_url+'upload/'+data.image+'" class="img-responsive" width="100" height="100">'); // show photo
                $('#photo-preview div').append('<input type="checkbox" name="remove_photo" value="'+data.image+'"/> Remove photo when saving'); // remove photo
 
            }
            else
            {
                $('#label-photo').text('Upload photo'); // label photo upload
                $('#photo-preview div').text('(No photo)');
            }
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
} 
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo site_url('admin/User_detail/ajax_add')?>";
    } else {
        url = "<?php echo site_url('admin/User_detail/ajax_update')?>";
    }
 
 	 var formData = new FormData($('#form')[0]);
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
		contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
				alert(data.message);
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}
 
function delete_user(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('admin/User_detail/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}
function bulk_delete()
{
    var list_id = [];
    $(".data-check:checked").each(function() {
            list_id.push(this.value);
    });
    if(list_id.length > 0)
    {
        if(confirm('Are you sure delete this '+list_id.length+' data?'))
        {
            $.ajax({
                type: "POST",
                data: {id:list_id},
                url: "<?php echo site_url('admin/User_detail/ajax_bulk_delete')?>",
                dataType: "JSON",
                success: function(data)
                {
                    if(data.status)
                    {
                        reload_table();
                    }
                    else
                    {
                        alert('Failed.');
                    }
                     
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
        }
    }
    else
    {
        alert('no data selected');
    }
}
  </script>
    <!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">User form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">First name:<span class="required" aria-required="true" style="color:red;">*</span></label>
                            <div class="col-md-9">
                                <input name="firstName" placeholder="First name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="control-label col-md-3">Last name:<span class="required" aria-required="true" style="color:red;">*</span></label>
                            <div class="col-md-9">
                                <input name="lastName" placeholder="Last name" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Email:<span class="required" aria-required="true" style="color:red;">*</span></label>
                            <div class="col-md-9">
                                <input name="email" placeholder="Email" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Password:<span class="required" aria-required="true" style="color:red;">*</span></label>
                            <div class="col-md-9">
                                <input name="password" placeholder="Password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Address:</label>
                            <div class="col-md-9">
                                <textarea name="address" placeholder="Address" class="form-control" ></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone number:<span class="required" aria-required="true" style="color:red;">*</span></label>
                            <div class="col-md-9">
                                <input name="mobile" placeholder="Phone number" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Role:<span class="required" aria-required="true" style="color:red;">*</span></label>
                            <div class="col-md-9">
                                <select name="role" id="role" class="form-control">
                            <option value="">----Select role----</option>
                            <?php 
					foreach ($role as $role): 
					$opt[]=$role->role_name; 

				echo '<option value="'.$role->role_id.'">'.$role->role_name.'</option>';
                           
					endforeach; 
	
				?>
                          </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                         <div class="form-group" id="photo-preview">
                            <label class="control-label col-md-3">Photo</label>
                            <div class="col-md-9">
                                (No photo)
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" id="label-photo">Upload photo:<span class="required" aria-required="true" style="color:red;">*</span> </label>
                            <div class="col-md-9">
                                <input name="image" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<?php $this->load->view('admin/commonfiles/footer'); ?>