<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Pramod Services|Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="<?php echo base_url();?>theme/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url();?>theme/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo base_url();?>theme/dist/css/skins/_all-skins.min.css">
<!-- Date Picker -->
<link rel="stylesheet" href="<?php echo base_url();?>theme/plugins/datepicker/datepicker3.css">
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>theme/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jquery validations -->
<script src="<?php echo base_url();?>theme/dist/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>theme/dist/js/moment.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<!----------------Data Tables style sheet and plugin --------------------------->
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/plugins/datatables/jquery.dataTables.css">
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>theme/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>theme/plugins/datatables/dataTables.colVis.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme/plugins/datatables/dataTables.colVis.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/plugins/colorbox/colorbox.min.css" />
<script type="text/javascript" src="<?php echo base_url();?>theme/plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>theme/plugins/ckeditor/ckeditor.js"></script>
<!------------------Breadcrumbs---------------------------------------------------------->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/breadcrumbs.css">
<!----------------End of Data Tables style sheet and plugin --------------------------->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fancybox/jquery.fancybox.min.css" />
<script src="<?php echo base_url(); ?>assets/fancybox/jquery.fancybox.min.js"></script>
<!------------------------------------------Chosen select script start------------------->
<link rel="stylesheet" href="<?php echo base_url();?>assets/search_dropdown_list_jquery_code/chosen.css">
<script src="<?php echo base_url(); ?>assets/search_dropdown_list_jquery_code/chosen.jquery.js"></script>
<!------------------------------------------Chosen select script end------------------->