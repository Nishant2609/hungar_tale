</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<header class="main-header">
  <!-- Logo -->
  <a href="<?php echo base_url()."admin/welcome"?>" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>HT</b></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>Hunger Tale</b></span> </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> 
    <span class="sr-only">Toggle navigation</span> </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <?php
		   $cid=$this->session->userdata('empid');
		   $this->db->select('image,first_name,last_name');
		   $this->db->from('tbl_admin_users');
		   $this->db->where('user_id',$cid);
		   $query = $this->db->get();
		   $result = $query->result();
		   if(isset($result) && sizeof($result)>0)
		   {
		   		$image=$result[0]->image;
				if($image=="")
				{
	    ?>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url();?>assets/images/user_image.png" class="user-image" alt="User Image"> <span class="hidden-xs"><?php echo $result[0]->first_name." ".$result[0]->last_name;?></span> </a>
          <?php
				}
				else
				{
	  ?>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url('uploads/'.$image );?>" class="user-image" alt="User Image" /> <span class="hidden-xs"><?php echo $result[0]->first_name." ".$result[0]->last_name;?></span> </a>
          <?php
				}
		   }
	  ?>
            
            
            <ul class="dropdown-menu">
            <!-- User image -->
            <?php
		   $cid=$this->session->userdata('empid');
		   $this->db->select('image,first_name,last_name');
		   $this->db->from('tbl_admin_users');
		   $this->db->where('user_id',$cid);
		   $query = $this->db->get();
		   $result = $query->result();
		   if(isset($result) && sizeof($result)>0)
		   {
		   		$image=$result[0]->image;
				if($image=="")
				{
	    ?>
            <li class="user-header"> <img src="<?php echo base_url();?>assets/images/user_image.png" width="100px" height="100px" class="img-circle"/>
              <p> <?php echo $result[0]->first_name." ".$result[0]->last_name;?> <small>Admin</small> </p>
            </li>
            <?php
				}
				else
				{
	  ?>
            <li class="user-header"> <img src="<?php echo base_url('uploads/'.$image );?>" width="100px" height="100px" class="img-circle"/>
              <p> <?php echo $result[0]->first_name." ".$result[0]->last_name;?> <small>Admin</small> </p>
            </li>
            <?php
				}
		   }
	  ?>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left"> <a href="<?php echo base_url(). "admin/Login/profile"?>" class="btn btn-default btn-flat">Profile</a> </div>
              <div class="pull-right"> <a href="<?php echo base_url(). "admin/Login/logout"?>" class="btn btn-default btn-flat">Sign out</a> </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
        <!--<li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
      </ul>
    </div>
  </nav>
</header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
      <?php
		   $cid=$this->session->userdata('empid');
		   $this->db->select('image,first_name,last_name');
		   $this->db->from('tbl_admin_users');
		   $this->db->where('user_id',$cid);
		   $query = $this->db->get();
		   $result = $query->result();
		   if(isset($result) && sizeof($result)>0)
		   {
		   		$image=$result[0]->image;
				if($image=="")
				{
	    ?>
        
        <div class="pull-left image"> <img src="<?php echo base_url();?>assets/images/user_image.png" class="user-image" alt="User Image" /> </div>
      <div class="pull-left info">
        <p><?php echo $result[0]->first_name." ".$result[0]->last_name;?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a> </div>
      <?php
				}
				else
				{
	  ?>
      				
              <div class="pull-left image"> <img src="<?php echo base_url('uploads/'.$image );?>" class="user-image" alt="User Image" /> </div>
      <div class="pull-left info">
        <p> <?php echo $result[0]->first_name." ".$result[0]->last_name;?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a> </div>
      <?php
				}
		   }
	  ?>
    </div>
      
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!--<li class="header">MAIN NAVIGATION</li>-->
        <li class="active treeview">
        <a href="<?php echo base_url()."admin/welcome"?>"> <i class="fa fa-tachometer"></i> 
        <span>Dashboard</span> </a>
    </a>
        </li>

        <li>
        <a href="<?php echo base_url()."admin/slider";?>">
        <i class="fa fa-file-image-o" aria-hidden="true"></i><span>Home page slider</span></a>
        </li>
        <li><a href="<?php echo base_url()."admin/food_types";?>">
        <i class="fa fa-archive" ></i><span>Food Types</span></a></li>       
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php /*?><section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()."admin/Login_ctrl/welcome"?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section><?php */?>

    <!-- Main content -->
    <section class="content">
     