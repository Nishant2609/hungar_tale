<?php $this->load->view('admin/commonfiles/header'); ?>
<?php $this->load->view('admin/commonfiles/menu'); ?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/breadcrumbs.css">
<style>
.user-search-input {
	width:100%;
}
</style>
<style>
label.error {
	width: 100%;
	color: red;
	font-style: italic;
	text-align:left;
	margin-bottom: 5px;
}
</style>
<script type="text/javascript">
$(document).on('click','.status_checks',function(){
      var status=($(this).hasClass("btn-success")) ? '0' : '1';
	   var msg=(status=='0')? 'Deactivate' : 'Activate';
	  
	  if(confirm("Are you sure to "+ msg)){
        var current_element=$(this);
		var id=$(current_element).attr('data');
	
		
       var myurl="<?php echo base_url()."admin/Services/update_status"?>";
        $.ajax({
          type:"POST",
          url:myurl,
          data:{"service_id":id,"status":status},
          success:function(data)
          {   
		  
            location.reload();
          }
        });
      }      
    });
</script>
<div class="row">
  <div class="col-lg-12" align="center"> <font size="+2" color="#0066CC" class="blinking">Service details</font> </div>
</div>
<ol class="breadcrumb breadcrumb-arrow">
  <li><a href="<?php echo base_url()."admin/welcome"?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>
  <li class="active"><span>Service details</span></li>
</ol>
<button class="btn btn-success" onclick="add_user()"><i class="glyphicon glyphicon-plus"></i> Add Service</button>
<button class="btn btn-danger" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk delete</button>
<br/>
<br/>
<div class="panel panel-info">
  <div class="panel-body">
    <div class="table-responsive">
      <table id="table" class="table table-striped table-bordered" cellpadding="0"  width="100%">
        <thead>
          <tr>
            <th><input type="checkbox" id="check-all"></th>
            <th>Sr.No.</th>
            <th>Name</th>
            <th>Description</th>
            <th>Image</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">

    var save_method; //for save method string
    var table;
	var base_url = '<?php echo base_url();?>';
    $(document).ready(function() {
      table = $('#table').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('admin/Services/ajax_list')?>",
            "type": "POST"
        },
		aLengthMenu: [
        [100,125, 150, 200, -1],
        [100,125, 150, 200, "All"]
    ],
        //Set column definition initialisation properties.
        "columnDefs": [
        {
          "targets": [ 0,-1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

		dom: 'lBfrtip',
		"buttons": [
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ]
		
      });
	  
	  		//	$("#table_filter").css("display","none");  // hiding global search box
				
				$('.user-search-input').on( 'keyup click change', function () {   
					var i =$(this).attr('id');  // getting column index
					var v =$(this).val();  // getting search input value
					table.columns(i).search(v).draw();
				} );
	   //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
	
	//check all
    $("#check-all").click(function () {
        $(".data-check").prop('checked', $(this).prop('checked'));
    });
 
});
function add_user()
	{
		save_method = 'add';
		$('#form')[0].reset(); // reset form on modals
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#modal_form').modal('show'); // show bootstrap modal
		$('.modal-title').text('Add Service'); // Set Title to Bootstrap modal title
		$('#photo-preview').hide(); // hide photo preview modal
 		$('#label-photo').text('Upload Photo'); // label photo upload
		
	}
function edit_user(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('admin/Services/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 			
            $('[name="id"]').val(data.service_id);
            $('[name="serviceName"]').val(data.service_name);
            //$('[name="serviceDescription"]').val(data.service_description);
			CKEDITOR.instances["serviceDescription"].setData(data.service_description);
			$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Service'); // Set title to Bootstrap modal title
 			$('#photo-preview').show(); // show photo preview modal
			$('#video-preview').show(); // show photo preview modal
 			
            if(data.service_image)
            {
                $('#label-photo').text('Change Photo'); // label photo upload
                $('#photo-preview div').html('<img src="'+base_url+'upload/service/'+data.service_image+'" class="img-responsive" width="100" height="100">'); // show photo
                $('#photo-preview div').append('<input type="checkbox" name="remove_photo" value="'+data.service_image+'"/> Remove photo when saving'); // remove photo
 
            }
            else
            {
                $('#label-photo').text('Upload Photo'); // label photo upload
                $('#photo-preview div').text('(No photo)');
            }
			
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
} 
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo site_url('admin/Services/ajax_add')?>";
    } else {
        url = "<?php echo site_url('admin/Services/ajax_update')?>";
    }
 	for ( instance in CKEDITOR.instances )
    CKEDITOR.instances[instance].updateElement();
 	 var formData = new FormData($('#form')[0]);
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
		contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
				alert(data.message);
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}
function delete_user(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('admin/Services/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}
function bulk_delete()
{
    var list_id = [];
    $(".data-check:checked").each(function() {
            list_id.push(this.value);
    });
    if(list_id.length > 0)
    {
        if(confirm('Are you sure delete this '+list_id.length+' data?'))
        {
            $.ajax({
                type: "POST",
                data: {id:list_id},
                url: "<?php echo site_url('admin/Services/ajax_bulk_delete')?>",
                dataType: "JSON",
                success: function(data)
                {
                    if(data.status)
                    {
                        reload_table();
                    }
                    else
                    {
                        alert('Failed.');
                    }
                     
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
        }
    }
    else
    {
        alert('no data selected');
    }
}
  </script>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Services Form</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
          <input type="hidden" value="" name="id"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Name:<span class="required" aria-required="true" style="color:red;">*</span></label>
              <div class="col-md-9">
                <input name="serviceName" placeholder="Service Name" class="form-control" type="text">
                <span class="help-block"></span> </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Description:</label>
              <div class="col-md-9">
                <textarea name="serviceDescription" placeholder="Service Description" class="form-control"></textarea>
                <script type="text/javascript">
                                    CKEDITOR.replace('serviceDescription');
                                    </script>
                <span class="help-block"></span> </div>
            </div>
            <div class="form-group" id="photo-preview">
              <label class="control-label col-md-3">Photo</label>
              <div class="col-md-9"> (No photo) <span class="help-block"></span> </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3" id="label-photo">Upload photo:</label>
              <div class="col-md-9">
                <input name="service_image" type="file">
                <span class="help-block"></span> </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Bootstrap modal -->
<?php $this->load->view('admin/commonfiles/footer'); ?>