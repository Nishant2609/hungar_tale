<?php $this->load->view('admin/commonfiles/header'); ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h2 class="panel-title" align="center"><b>Enquiry details</b></h2>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table  class="table table-striped" cellpadding="0">
        <?php
			if(isset($enquiry_details))
			{
			foreach($enquiry_details as $enquiry_det)
			{ 
			?>
        <tr>
          <th>Enquiry id</th>
          <td><?php echo $enquiry_det->enquiry_generated_id;?></td>
          <th>Enquiry type</th>
          <td><?php if($enquiry_det->enquiry_type_id==1)
								{echo "Service";}else{echo "Product";}?></td>
        </tr>
        <tr>
          <th>Name</th>
          <td><?php echo $enquiry_det->enquiry_name;?></td>
          <th>Email</th>
          <td><?php echo $enquiry_det->enquiry_email;?></td>
        </tr>
        <tr>
          <th>Mobile</th>
          <td><?php echo $enquiry_det->enquiry_mobile;?></td>
          <th>Area</th>
          <td><?php echo $enquiry_det->enquiry_subject;?></td>
        </tr>
        <tr>
          <th>Enquiry date</th>
          <td><?php echo date("d-m-Y", strtotime($enquiry_det->added_date));?></td>
        </tr>
        <tr>
          <th>Enquiry message</th>
          <td><?php echo $enquiry_det->enquiry_details;?></td>
        </tr>
        <?php
			}
			}
			?>
      </table>
    </div>
  </div>
</div>
