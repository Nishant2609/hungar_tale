<?php $this->load->view('admin/commonfiles/header'); ?>
<div class="panel panel-info"> 
 	<div class="panel-heading">
          <h2 class="panel-title" align="center"><b>Item details</b></h2>   
    </div>   
      <div class="panel-body"> 
<div class="table-responsive">
    <table  class="table table-striped" cellpadding="0">
    		<?php
			if(isset($item_details))
			{
			foreach($item_details as $item_det)
			{ 
			?>			
            			<tr>
           						<th>Name</th>
                                <td><?php echo $item_det->item_name;?></td>
                                <th>Price</th>
                                <td><?php echo $item_det->item_price;?></td>
                         </tr>
                         <tr>
           						<th>Tax</th>
                                <td><?php echo $item_det->item_tax;?></td>
                                <th>Quantity</th>
                                <td><?php echo $item_det->item_quantity;?></td>
                         </tr>
                         
            			<tr>
           						<th>Added date</th>
                                <td><?php echo date("d-m-Y", strtotime($item_det->added_date));?></td>
                                <th>Modified date</th>
                                <td><?php echo date("d-m-Y", strtotime($item_det->modified_date));?></td>
                         </tr>
                         <tr>
                                <th>Description</th>
                                 <td><?php echo $item_det->item_description;?></td>
                                
                         </tr>
                      
            <?php
			}
			}
			?>
    </table>
</div>
</div>
</div>