<?php $this->load->view('admin/commonfiles/header'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/profile.js"></script>
<?php $this->load->view('admin/commonfiles/menu'); ?>

<style>
.btn-glyphicon { padding:8px; background:#ffffff; margin-right:4px; }
.icon-btn { padding: 1px 15px 3px 2px; border-radius:50px;}
#showHide {
  width: 15px;
  height: 15px;
  float: left; margin-top: 10px;
}
#showHideLabel {
  float: left;
  padding-left: 5px;padding-top:8px;
}
input.error {
  border: 1px dotted red;
}
label.error {
  width: 100%;
  color: red;
  font-style: italic;
  text-align:left;
  margin-bottom: 5px;
}
</style>
<script>
$(document).ready(function() {
  $("#showHide").click(function() {
    if ($(".password").attr("type") == "password") {
      $(".password").attr("type", "text");

    } else {
      $(".password").attr("type", "password");
    }
  });
});
</script>
<div id="wrapper">
  <div id="page-content-wrapper">
    <div class="container-fluid">
      <div align="right"> <a class="btn icon-btn btn-info"  href="<?php echo base_url()."admin/Login/profile"?>"><span class="glyphicon btn-glyphicon glyphicon-arrow-left img-circle text-info"></span>Back</a> </div>
      <br/>
     <div class="container">
	<div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="well well-sm">
          <form class="form-horizontal" enctype="multipart/form-data" name="profileeditForm" id="profileeditForm" action="<?php echo base_url()."admin/Login/update_profile"?>" method="post">
          <fieldset>
            <legend class="text-center"><h2 style="margin-top:5px;"> Update profile details </h2></legend>
    		 <?php 
				foreach($records as $record)
				{
				 echo form_hidden('id',$record->user_id); 
				?>
            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">First name : <span class="required" aria-required="true" style="color:red;">*</span></label>
              <div class="col-md-8">
                <input id="firstname" name="firstname" type="text" placeholder="Your first name" class="form-control" value="<?php echo $record->first_name; ?>">
              </div>
            </div>
    
    		<div class="form-group">
              <label class="col-md-3 control-label" for="name">Last Name : <span class="required" aria-required="true" style="color:red;">*</span></label>
              <div class="col-md-8">
                <input id="lastname" name="lastname" type="text" placeholder="Your last name" class="form-control" value="<?php echo $record->last_name ?>">
              </div>
            </div>
            
           
            <!-- Email input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="email">Email : <span class="required" aria-required="true" style="color:red;">*</span></label>
              <div class="col-md-8">
                <input id="email" name="email" type="text" placeholder="Your email" class="form-control" value="<?php echo $record->email ?>">
              </div>
            </div>
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="password">Password : <span class="required" aria-required="true" style="color:red;">*</span></label>
              <div class="col-md-8">
                <input type="password" id="password" name="password" placeholder="Your password" class="form-control password" value="<?php echo $record->password;?>" />
                   <input type="checkbox" id="showHide" />
      <label for="showHide" id="showHideLabel">Show password</label>
              </div>
            </div>
    			
             
            
            
            
             <div class="form-group">
              <label class="col-md-3 control-label" for="mobile"> Mobile number : <span class="required" aria-required="true" style="color:red;">*</span></label>
              <div class="col-md-8 ">
               <!--<input id="mobile" name="mobile" type="text" placeholder="Your Mobile Number" class="form-control" value="<?php echo $record->mobile ?>">-->
               <div class="input-group">
                           <span class="input-group-addon">+91</span>
               <input id="mobile" name="mobile" type="text" placeholder="Your mobile number" class="form-control" value="<?php echo $record->mobile ?>">                                         
                        </div>
              </div>
            </div>
            
             <div class="form-group">
              <label class="col-md-3 control-label" for="profile" style=" padding-right: 25px;">Your Profile Image :
              </label>
              <div class="col-md-8">
                <?php
                    if($record->image!='')
                    {
						
                    ?>
                   <img src="<?php echo base_url('upload/'.$record->image);?>" width="50px" height="50px" /> <a href="<?php echo base_url();?>admin/Login/update_image?id=<?php echo $record->user_id;?>&img=<?php echo $record->image;?>">Change</a>
                    <input type="hidden" name="image" id="image" value="<?php echo $record->image;?>">
                  
                
                <?php
					}
					else
					{
					?>
                
                  <div class="col-md-6">
                  <label class="custom-file">
                    <input type="file" name="image" id="image" value="">
                    <span class="custom-file-control"></span>
                    </label>
                  </div>
                  <?php
					}
					
					?>
              </div>
            </div>
            <!-- Form actions -->
            
              <div class="col-md-12">
              <div class="row" style=" float:right; margin-right:20px; margin-right: 50px;">
                <button type="submit" class="btn btn-primary" name="btn_submit" id="btn_submit"onclick="return confirm('Are you sure you want to save this details?');">Update</button>
                </div>
              </div>
          
              <?php
				
				}
				?>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div>

               
       
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('admin/commonfiles/footer'); ?>	
			
		