<?php $this->load->view('admin/commonfiles/header'); ?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/breadcrumbs.css">
<?php $this->load->view('admin/commonfiles/menu'); ?>
<style>
.popupunder {
	width: 300px;
	position:fixed;
	top: 60px;
	right: 10px;
	z-index: 10;
	border: 0;
	padding: 20px;
}
.popupunder.alert-success {
	border: 1px solid #198b49;
	background:#27AE60;
	color:#fff;
}
.popupunder .close {
	font-size: 10px;
	position:absolute !important;
	right: 2px;
	top: 3px;
}
</style>
<style>
label.error {
	width: 100%;
	color: red;
	font-style: italic;
	text-align:left;
	margin-bottom: 5px;
}
</style>
<style type="text/css">
.paging-nav {
	text-align: right;
	padding-top: 2px;
}
.paging-nav a {
	margin: auto 1px;
	text-decoration: none;
	display: inline-block;
	padding: 1px 7px;
	background: #91b9e6;
	color: white;
	border-radius: 3px;
}
.paging-nav .selected-page {
	background: #187ed5;
	font-weight: bold;
}
.paging-nav, #tableData {
	margin: 0 auto;
	font-family: Arial, sans-serif;
}
</style>
<!-- Script for Multiple selection through checkbox -->
<script type="text/javascript">
$(document).ready(function(){
    $('#select_all').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.checkbox').on('click',function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });
});
</script>
<!-- Script for Active In-active Perticular record -->
<script type="text/javascript">
 
					var save_method; //for save method string
					var table;
					 
					$(document).ready(function() {
					 
						//datatables
						table = $('#table').DataTable({ 
					 
							"processing": true, //Feature control the processing indicator.
							"serverSide": true, //Feature control DataTables' server-side processing mode.
							"order": [], //Initial no order.
					 
							// Load data for the table's content from an Ajax source
							"ajax": {
								"url": "<?php echo base_url()."admin/Contactus/ajax_list"?>",
								"type": "POST"
							},
					 		aLengthMenu: [
        [100,125, 150, 200, -1],
        [100,125, 150, 200, "All"]
    ],
							//Set column definition initialisation properties.
							"columnDefs": [
							{ 
								"targets": [ 0 ], //last column
								"orderable": false, //set not orderable
							},
							],
					 
						});
					 
					   var colvis = new $.fn.dataTable.ColVis(table); //initial colvis
						$('#colvis').html(colvis.button()); //add colvis button to div with id="colvis"
					 
						
						
						//set input/textarea/select event when change value, remove class error and remove text help block 
						$("input").change(function(){
							$(this).parent().parent().removeClass('has-error');
							$(this).next().empty();
						});
						$("textarea").change(function(){
							$(this).parent().parent().removeClass('has-error');
							$(this).next().empty();
						});
						$("select").change(function(){
							$(this).parent().parent().removeClass('has-error');
							$(this).next().empty();
						});
					 
					});
					 
					
					 
					function reload_table()
					{
						table.ajax.reload(null,false); //reload datatable ajax 
					}
					 
					
					 
					function delete_subject(contactus_id)
					{
						if(confirm('Are you sure delete this data?'))
						{
							// ajax delete data to database
							$.ajax({
								url : "<?php echo base_url()."admin/Contactus/ajax_delete/"?>"+contactus_id,
								type: "POST",
								dataType: "JSON",
								success: function(data)
								{
									//if success reload ajax table
									$('#modal_form').modal('hide');
									reload_table();
								},
								error: function (jqXHR, textStatus, errorThrown)
								{
									alert('Error deleting data');
								}
							});
					 
						}
					}
 
			</script>
<?php
                if($this->session->flashdata('deletemsgs')) {
                   ?>
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <div class="popupunder alert alert-danger fade in" id="success-alert4">
        <button type="button" class="close close-sm" data-dismiss="alert"><i class="glyphicon glyphicon-remove"></i></button>
        <?php
                echo ''.$this->session->flashdata('deletemsgs').'';?>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<!-- Display all country in Grid format -->
<div id="wrapper">
  <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12" align="center"> <font size="+2" color="#0066CC" class="blinking">Contact us</font> </div>
      </div>
      <br />
      <br/>
      <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="<?php echo base_url()."admin/welcome"?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>
        <li class="active"><span>Contact us</span></li>
      </ol>
      <div class="panel panel-info">
        <div class="panel-body">
          <div class="table-responsive">
            <form method="post" action="<?php echo base_url()."admin/Contactus/delete_checkbox";?>">
              <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th> <input type="checkbox" id="select_all" value="Select all" />
                    </th>
                    <th> Contact us name </th>
                    <th> Contact us email </th>
                    <th> Contact us description </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody >
                </tbody>
              </table>
              <input type="submit" name="submit" value="Delete selected items" class="btn btn-danger" id="btn_submit1">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End of Contactus Grid format-->
<!-- /.modal -->
<?php $this->load->view('admin/commonfiles/footer'); ?>
