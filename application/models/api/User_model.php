<?php

class User_model extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->database();
  }

  public function get_students(){

    $this->db->select("*");
    $this->db->from("tbl_users");
    $query = $this->db->get();

    return $query->result();
  }

   public function insert_student($data = array()){

       return $this->db->insert("tbl_users", $data);
   }

   public function delete_student($student_id){

     // delete method
     $this->db->where("id", $student_id);
     return $this->db->delete("tbl_users");
   }

   public function update_student_information($id, $informations){

      $this->db->where("id", $id);
      return $this->db->update("tbl_users", $informations);
   }


// Fetch country code

    public function get_country_code($country_name){

    $this->db->select("phonecode");
    $this->db->from("tbl_country");
    $this->db->where("nicename", $country_name);
    $query = $this->db->get();

    return $query->result();
  }
   // fetch mobile number 

  public function get_mobile_no($mobile){

    $this->db->select("mobile,user_type");
    $this->db->from("tbl_users");
     $this->db->where("mobile", $mobile);
    $query = $this->db->get();
     

    if($query->num_rows() == 1){
        $data = $query->row_array();
                $data['otp'] = rand(1000, 9999);
                 $data['mobile'] = $mobile;
      } else {
                $data['otp'] = rand(1000, 9999);
                 $data['mobile'] = $mobile;
                 $data['user_type'] = 2;

      }
    
        return $data;

    //return $query->result();
  }
  // Add user
  public function insert_user($data = array()){

        $this->db->insert("tbl_users", $data);

       $user_id=$this->db->insert_id();
       $this->db->set('user_type', '1');
       $this->db->where("user_id", $user_id);
      return $this->db->update("tbl_users");
      
   }

   // fetch user details by mobile number 

  public function get_user_details($mobile){

    $this->db->select("user_id,first_name,last_name,email,mobile,user_type,profile_image");
    $this->db->from("tbl_users");
     $this->db->where("mobile", $mobile);
    $query = $this->db->get();
     
    //$data=null;
    if($query->num_rows() == 1){
        $data = $query->row_array(); 
        return $data;              
      }
    
        

    //return $query->result();
  }
 
  // Fetch slider images

  public function get_slider_images(){

    $this->db->select("image_url");
    $this->db->from("tbl_slider_images");
    $this->db->where("is_home ", 0);
    
    $query = $this->db->get();

    return $query->result();
  }

  // Fetch slider images

  public function get_slider_home(){

    $this->db->select("image_url");
    $this->db->from("tbl_slider_images");
    $this->db->where("is_home ", 1);
    
    $query = $this->db->get();

    return $query->result();
  }

   // Fetch list of popular and all cuisines

   public function get_popular_cuisines(){

    $this->db->select("type_name");
    $this->db->from("tbl_food_type");
    $this->db->where("is_popular", 1);
    
    $query = $this->db->get();

    return $query->result();
  }
  public function get_all_cuisines(){

    $this->db->select("type_name");
    $this->db->from("tbl_food_type");
   
    
    $query = $this->db->get();

    return $query->result();
  }

   public function update_user_information($id, $informations){

    $this->db->where("user_id", $id);
    return $this->db->update("tbl_users", $informations);
 }

  //where it's end

}

 ?>
