<?php
class User_detail_model extends CI_Model
{	

	 var $table = 'tbl_admin_users';
    var $column_order = array(null,'first_name','last_name','email','address','mobile','role_name','image','status',null); //set column field database for datatable orderable
    var $column_search = array('first_name','last_name','email','address','mobile','role_name'); //set column field database for datatable searchable 
    var $order = array('user_id'=>'asc'); // default order 
	
	function __construct() 
	 {
		parent::__construct();
	 }
	 
	 
	 private function _get_datatables_query()
    {
        $this->db->select('tbl_admin_users.*,psr_role.role_name');  
        $this->db->from($this->table);
		$this->db->join('psr_role', 'psr_role.role_id = psr_users.role_id');
		$this->db->where('psr_users.is_deleted',0);
        $i = 0;
		
		
		
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
		$this->_get_custom_field();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
		$this->_get_custom_field();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	
	function _get_custom_field()
    {
        if(isset($_POST['columns'][1]['search']['value']) and $_POST['columns'][1]['search']['value'] !='')
		{
           $this->db->like('psr_users.first_name',$_POST['columns'][1]['search']['value']);   
        }
		
		if(isset($_POST['columns'][2]['search']['value']) and $_POST['columns'][2]['search']['value'] !='')
		{
		    $this->db->like('psr_users.last_name',$_POST['columns'][2]['search']['value']);
        }
		
		if(isset($_POST['columns'][3]['search']['value']) and $_POST['columns'][3]['search']['value'] !='')
		{
           $this->db->like('psr_users.email',$_POST['columns'][3]['search']['value']);
        }
		
		if(isset($_POST['columns'][4]['search']['value']) and $_POST['columns'][4]['search']['value'] !='')
		{
           $this->db->like('psr_users.address',$_POST['columns'][4]['search']['value']);
        }
		
		if(isset($_POST['columns'][5]['search']['value']) and $_POST['columns'][5]['search']['value'] !='')
		{
           $this->db->like('psr_users.mobile',$_POST['columns'][5]['search']['value'], 'after');
        }
    }
	
	
	public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('user_id',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
 
    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
 
    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id)
    {
		$data = array( 
						'is_deleted' => 1
						);
		$this->db->set($data);
        $this->db->where('user_id', $id);
		$this->db->update($this->table,$data);
    }
	public function show_student()
					{
							$query = $this->db->get('psr_users');
							$query_result = $query->result();
							return $query_result;
					}
	 
	public function update_status($c_id,$status)
					{
  							$data['status'] = $status;
  							$this->db->where('user_id', $c_id);
  							$this->db->update('psr_users',$data);
					} 
	public function show_roles()
					{
							$this->db->where('psr_role.status','1');
							$query = $this->db->get('psr_role');						
							$query_result = $query->result();
							return $query_result;
					}

}
?>