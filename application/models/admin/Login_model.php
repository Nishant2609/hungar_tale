<?php
class Login_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}
	
	// Function To Fetch All Students Record
	function login_user($email,$password)
	{
				$where= array(
					   'email'=>$email,
					   'password'=>$password,
					   'status'=>'1',
					   'role_id'=>'1'
					  
					  );
				$query = $this->db->get_where('tbl_admin_users', $where);
				
				if($query -> num_rows() == 1)
				{
					return $query->result();
				}
				else
				{
					$where= array(
					   'email'=>$email,
					   'password'=>$password,
					   'status'=>'1',
					   'role_id'=>'2'
					  
					  );
					$query = $this->db->get_where('tbl_admin_users', $where);
					
					if($query -> num_rows() == 1)
					{
						return $query->result();
					}
					else
					{
						return false;
					}
				}
		}
		
	function inser_session_data($session_data)
	{
		$this->db->insert('psr_user_session',$session_data);
		
	}
	function show_user_id($email,$password)
	{
		$where= array(
               'email'=>$email,
               'password'=>$password
              );
		$this->db->select('*');
		$this->db->from('tbl_admin_users');
		$this->db->where($where);
		$this->db->where('is_deleted',0);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	
	function form_insert($data)
	{
		
		$this->db->insert('tbl_admin_users', $data);
		
	}		
	
	public function getemployeedetails($uid)
					{
	                     //get all the employee details from the tbl_admin_users table  
                        $this->db->select('tbl_admin_users.*');
						$this->db->from('tbl_admin_users');
						$this->db->where('tbl_admin_users.is_deleted',0);
						$this->db->where('user_id',$uid);
						$query = $this->db->get();
		                $query_result = $query->result();
						return $query_result;
					
					}
		function show_user($id)
					{
						$this->db->select('tbl_admin_users.*');
						$this->db->from('tbl_admin_users');
						$this->db->where('tbl_admin_users.is_deleted',0);
						$this->db->where('user_id', $id);
						$query = $this->db->get();
						$result = $query->result();
						return $result;
					}
					
		public function update($data,$id) 
				   { 
					
					 $this->db->set($data); 
					 $this->db->where("user_id", $id); 
					 $this->db->update("tbl_admin_users", $data); 
				   } 
				   
		 function update_image_id($id)
					{
					        //update the image for perticular employee from tbl_admin_users
							$this->db->set('image', "");
							$this->db->where('user_id', $id);
							$this->db->update('tbl_admin_users');		
					}	
    }
?>
