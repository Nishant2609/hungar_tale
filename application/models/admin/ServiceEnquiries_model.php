<?php
class ServiceEnquiries_model extends CI_Model
{	

	 var $table = 'psr_enquiries';
    var $column_order = array(null,'enquiry_generated_id','enquiry_name','enquiry_email','enquiry_mobile','enquiry_subject','enquiry_details','added_date','modified_date','status',null); //set column field database for datatable orderable
    var $column_search = array('enquiry_generated_id','enquiry_name','enquiry_email','enquiry_mobile','enquiry_subject','enquiry_details','added_date','modified_date'); //set column field database for datatable searchable 
    var $order = array('id'=>'asc'); // default order 
	
	function __construct() 
	 {
		parent::__construct();
	 } 
	 
	private function _get_datatables_query()
    {
        $this->db->select('psr_enquiries.*');  
        $this->db->from($this->table);
		$this->db->where('psr_enquiries.is_deleted',0);
		$this->db->where('psr_enquiries.enquiry_type_id','1');
		$this->db->order_by('id', "desc");
        $i = 0;
		
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	
	public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
 
    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
 
    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id)
    {
		$data = array( 
						'is_deleted' => 1
						);
		$this->db->set($data);
        $this->db->where('id', $id);
		$this->db->update($this->table,$data);
    }
	
	 
	public function update_status($c_id,$status)
	{
  		//update status
		$data['status'] = $status;
  		$this->db->where('id',$c_id);
  		$this->db->update('psr_enquiries',$data);
	} 
	
	public function show_clients()
	{
		$this->db->where('psr_clients.status','1');
		$this->db->where('psr_clients.is_deleted','0');
		$query = $this->db->get('psr_clients');						
		$query_result = $query->result();
		return $query_result;
	}
	public function get_enquiry_details($enquiry_id)
	{
		$this->db->select('psr_enquiries.*');  
        $this->db->from($this->table);
		$this->db->where('id',$enquiry_id);
		$query = $this->db->get();						
		$query_result = $query->result();
		return $query_result;
	}
}
?>