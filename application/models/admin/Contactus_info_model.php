<?php
class Contactus_info_model extends CI_Model
{	

	 var $table = 'psr_contactus_info';
    var $column_order = array(null,'contactus_info_address','contactus_info_phone1','contactus_info_phone2','contactus_info_phone3','contactus_info_email','status',null); //set column field database for datatable orderable
    var $column_search = array('contactus_info_address','contactus_info_phone1','contactus_info_phone2','contactus_info_phone3','contactus_info_email'); //set column field database for datatable searchable 
    var $order = array('contactus_info_id'=>'desc'); // default order 
	
	function __construct() 
	 {
		parent::__construct();
	 }
	 
	 
	 private function _get_datatables_query()
    {
        
        $this->db->from($this->table);
		$this->db->where('psr_contactus_info.is_deleted',0);
		$this->db->order_by('psr_contactus_info.contactus_info_id','desc');
        $i = 0;
		
		
		
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
		$this->_get_custom_field();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
		$this->_get_custom_field();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	
	function _get_custom_field()
    {
        if(isset($_POST['columns'][1]['search']['value']) and $_POST['columns'][1]['search']['value'] !='')
		{
           $this->db->like(' psr_contactus_info.contactus_info_address',$_POST['columns'][1]['search']['value']);   
        }
		
		if(isset($_POST['columns'][2]['search']['value']) and $_POST['columns'][2]['search']['value'] !='')
		{
		    $this->db->like(' psr_contactus_info.contactus_info_phone1',$_POST['columns'][2]['search']['value']);
        }
		
		
    }
	
	
	public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('contactus_info_id',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
 
    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
 
    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id)
    {
		$data = array( 
						'is_deleted' => 1
						);
		$this->db->set($data);
        $this->db->where('contactus_info_id', $id);
		$this->db->update($this->table,$data);
    }
	public function show_student()
					{
							$query = $this->db->get('psr_contactus_info');
							$query_result = $query->result();
							return $query_result;
					}
	 
	public function update_status($c_id,$status)
					{
  							$data['status'] = $status;
  							$this->db->where('contactus_info_id', $c_id);
  							$this->db->update('psr_contactus_info',$data);
					} 
	public function show_roles()
					{
							$this->db->where('psr_role.status','1');
							$query = $this->db->get('psr_role');						
							$query_result = $query->result();
							return $query_result;
					}

}
?>